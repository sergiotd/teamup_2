<?php

$lang['cal_su']			= "Do";
$lang['cal_mo']			= "Se";
$lang['cal_tu']			= "Te";
$lang['cal_we']			= "Qu";
$lang['cal_th']			= "Qu";
$lang['cal_fr']			= "Se";
$lang['cal_sa']			= "Sa";
$lang['cal_sun']		= "Dom";
$lang['cal_mon']		= "Seg";
$lang['cal_tue']		= "Ter";
$lang['cal_wed']		= "Qua";
$lang['cal_thu']		= "Qui";
$lang['cal_fri']		= "Sex";
$lang['cal_sat']		= "Sab";
$lang['cal_sunday']		= "Sunday";
$lang['cal_monday']		= "Monday";
$lang['cal_tuesday']	= "Tuesday";
$lang['cal_wednesday']	= "Wednesday";
$lang['cal_thursday']	= "Thursday";
$lang['cal_friday']		= "Friday";
$lang['cal_saturday']	= "Saturday";
$lang['cal_jan']		= "Jan";
$lang['cal_feb']		= "Feb";
$lang['cal_mar']		= "Mar";
$lang['cal_apr']		= "Apr";
$lang['cal_may']		= "May";
$lang['cal_jun']		= "Jun";
$lang['cal_jul']		= "Jul";
$lang['cal_aug']		= "Aug";
$lang['cal_sep']		= "Sep";
$lang['cal_oct']		= "Oct";
$lang['cal_nov']		= "Nov";
$lang['cal_dec']		= "Dec";
$lang['cal_january']	= "January";
$lang['cal_february']	= "February";
$lang['cal_march']		= "March";
$lang['cal_april']		= "April";
$lang['cal_mayl']		= "May";
$lang['cal_june']		= "June";
$lang['cal_july']		= "July";
$lang['cal_august']		= "August";
$lang['cal_september']	= "September";
$lang['cal_october']	= "October";
$lang['cal_november']	= "November";
$lang['cal_december']	= "December";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */