<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();


	}
		
	function insert($table_name = '', $data = array())
	{
		
		if($this->db->insert($table_name, $data))
		{

		    return $this->db->insert_id();

		}else{

			return FALSE;

		}
	
	}

	function get_rows($table_name = '', $data = array())
	{

		$this->db->from($table_name);

		if (!empty($data)) {

			$this->db->where($data);

		}
		
		return $this->db->get();

	}

	function get_id($table_name = '', $data = array())
	{

		$this->db->select('id');

		$this->db->from($table_name);

		$this->db->where($data);

		$result = $this->db->get()->result_array();

		if (! empty($result)) {

			return $result[0]['id'];

		} else {

			return FALSE;

		}
	
	}

	function update($table_name = '', $data = array(), $update = array())
	{

		$this->db->where($update);
		

		if ($this->db->update($table_name, $data)) {

			return TRUE;

		} else {

			return FALSE;

		}
		
	}

	function get_country_list()
	{

		$values = $this->db->get('countries')->result_array();

		$dropdown_values = array_to_dropdown($values, 'id', 'country');

		return $dropdown_values;

	}

	function get_languages_list()
	{

		$values = $this->db->get('languages')->result_array();

		$dropdown_values = array_to_dropdown($values, 'id', 'language');

		return $dropdown_values;

	}

	function get_faixas_etarias_list()
	{

		$values = $this->db->get('faixas_etarias')->result_array();

		$array_temp = array_to_dropdown($values, 'id', array('min', 'max'));

		$dropdown_values = array();

		foreach ($array_temp as $key => $value) {

			$dropdown_values[$key] = $value['min'] . ' - ' . $value['max'];

		}

		return $dropdown_values;

	}

	function get_social_networks_list()
	{

		$values = $this->db->get('social_networks')->result_array();

		$dropdown_values = array_to_dropdown($values, 'id', 'network');

		return $dropdown_values;

	}

	function insert_if_not_exists($table_name = '', $data = array())
	{


		

		$obj = $this->get_id($table_name, $data);



		if ($obj === FALSE) {

			$id = $this->insert($table_name, $data);

			return $id;

		} else {

			return $obj;

		}
		
	}

}