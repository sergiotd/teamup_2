<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	var $logged_in = FALSE;


	public function __construct()
	{
		parent::__construct();

		// if ($this->input->post()) {

		// 	initialize_controller();

		// }
	   
	   
	   // header('HTTP/1.1 403 Forbidden');

	   // if (current_url() != site_url('')) {
	   // 	redirect(site_url(''));
	   // }

	   // $this->session->set_userdata('languages_id', '6');

	   if (!isset($_SERVER['HTTP_REFERER'])) {
	   	// die();
	   }

	   $this->load->library('session');

	   

		if ($this->session->userdata('logged_in') === FALSE) {

	   		$this->session->set_userdata('logged_in', FALSE);

		}
	   
	
	}

	public function initialize_controller()
	{

		foreach ($this->input->post() as $key => $value) {

			if (isset($this->$key)) {
				$this->$key = $value;
			}
			
		}

	}



}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */	