<?php if (!empty($loners_list)): ?>
	
	<?php foreach ($loners_list as $key => $value): ?>

		<div class="row-fluid">

			<div class="span1">

				<?php echo form_checkbox('loner', '', FALSE, 'class="passive_checkbox"'); ?>

			</div>

			<div class="span11" data-teamid="<?php echo $value['users_id'] ?>">

				<?php echo anchor('user_profile', $value['avatar'], 'class="passive_user"'); ?>

			</div>

		</div>
		
	<?php endforeach ?>

<?php endif ?>