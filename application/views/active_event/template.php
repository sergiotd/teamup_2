<div class="row-fluid">

	<div class="span4">

		<label class="control-label" for="input01">Maximo:</label>

		<span class="uneditable-input">xx</span>

	</div>

	<div class="span4">

		<label class="control-label" for="input01">Confirmados:</label>

		<span class="uneditable-input">xx</span>

	</div>

	<div class="span4">

		<label class="control-label" for="input01">Vagas:</label>

		<span class="uneditable-input">xx</span>

	</div>			

</div>

<div class="row-fluid">

	<label class="control-label" for="input01">Participantes</label>

	<?php for($i=1; $i<10; $i++):?>

		<div class="row-fluid">

			<div class="span2">

				<?php echo '#' . $i ?>

			</div>

			<div class="span5">

				<span class="input-xlarge uneditable-input">

					Nome do Utilizador

				</span>

			</div>

			<div class="span1">Amigo</div>

			<div class="span3">

				<?php $this->load->view('star_ranking/template') ?>
					
			</div>

		</div>

	<?php endfor; ?>

</div>

<?php $this->load->view('abrir_vagas_publicas/template') ?>

<label class="control-label" for="input01">Publico</label>

<?php for($i=1; $i<10; $i++):?>

	<div class="row-fluid">

		<div class="span2">

			<div class="btn-group">

				<button class="btn btn-mini"><i class="icon-ok"></i></button>

				<button class="btn btn-mini"><i class="icon-remove"></i></button>

				<button class="btn btn-mini active_event_view_user_profile"><i class="icon-eye-open"></i></button>

			</div>	

		</div>

		<div class="span6">

			<span class="input-xlarge uneditable-input">

				Nome do Utilizador

			</span>

		</div>

		<div class="span4">

			<?php $this->load->view('star_ranking/template') ?>
				
		</div>

	</div>

<?php endfor; ?>

