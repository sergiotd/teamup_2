<?php echo validation_errors(); ?>

<form action="<?php echo site_url('log_in/submit') ?>" class="form-horizontal" id="log_in_form" method="POST">

	<label>Email</label>

	<input type="text" name="email" value="<?php echo set_value('email'); ?>" placeholder="Please Type your email">

	<label>Password</label>
	
	<input type="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Please Type your Password">

</form>