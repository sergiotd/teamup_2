<div id="form_errors"></div>

<?php echo form_open('creator/novo_evento_activo_submit', 'class="form-horizontal" id="form_novo_evento_activo"'); ?>

	<?php echo form_fieldset('What') ?>

		<div class="control-group">

			<div class="controls">

				<div id="search_sport"></div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('Where') ?>
	
		<div class="control-group">

			<div class="controls">

				<div class="span12" id="search_location"></div>

			</div>

		</div>

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="map_canvas"></div>

			</div>

		</div>		

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="map_address_viewer"></div>

			</div>

		</div>

		<div class="control-group">

			<?php  
				$attr = array(

					'class' => 'control-label'

					);

				echo form_label('Nome do Local', 'nome_do_local_label', $attr);

			?>

			<div class="controls">

				<?php echo form_input('local_nome', '', 'id="local_nome" class="span12"'); ?>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('When') ?>

		<div class="control-group">

			<div class="controls">

				<div id="date_chooser"></div>

			</div>

		</div>

		<div class="control-group">

			<div class="controls">

				<div id="time_chooser"></div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('Jogadores') ?>

		<div class="control-group">

			<?php 

				$attr = array(

					'class' => 'control-label'

					);

				echo form_label('# maximo de jogadores','max_jogadores_label', $attr);

			?>

			<div class="controls">

				<?php echo form_input('max_jogadores', '', 'class="span2"'); ?>

			</div>

		</div>

		<div class="control-group">

			<?php 

				$attr = array(

					'class' => 'control-label'

					);

				echo form_label('Tipo de Participação', 'tipos_de_participacao_label', $attr);

			?>

			<div class="controls">

				<?php echo form_dropdown('tipos_de_participacao_dropdown', $tipos_de_participacao, '0'); ?>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<div id="evento_privado" style="display: none">

		<?php echo form_fieldset('Vagas Privadas') ?>

			<div class="control-group">

				<div class="controls">

					<?php 

						$data = array(

						    'name'    => 'invite_friends',

						    'value'   => 'invite_friends',

						    'type'    => 'button',

						    'content' => 'Invite Friends',

						    'class'   =>'btn'

						);

						echo form_button($data);

					?>

				</div>

			</div>

			<div class="control-group">

				<?php 

					$attr = array(

						'class' => 'control-label'

						);

					echo form_label('Notas Privadas', 'notas_privadas', $attr);

				?>

				<div class="controls">

					<?php echo form_textarea('notas_evento_privado', '', 'class="span12" style="resize: none"'); ?>

				</div>

			</div>

		<?php echo form_fieldset_close() ?>

	</div>

	<div id="evento_publico" style="display: none">

		<?php echo form_fieldset('Vagas Publicas') ?>

		<br>

			<div id="evento_only_public">

				

				<div class="control-group">

					<?php

						$attributes = array(
						    'class' => 'control-label',
						    
						);
						echo form_label('Vagas publicas automaticas', 'vagas_publicas_automaticas_label', $attributes);


					?>

					<div class="controls">

						<?php echo form_checkbox('abrir_vagas_publicas_automaticas', 'ok', FALSE); ?>

					</div>

				</div>		

				<div class="control-group" id="num_vagas_publicas_div">

					<?php

						$attributes = array(
						    'class' => 'control-label',
						    
						);
						echo form_label('# Vagas Publicas', 'num_vagas_publicas', $attributes);


					?>

					<div class="controls">

						<?php echo form_input('num_vagas_publicas', '', 'class="span2"'); ?>

					</div>

				</div>

				<br>

			</div>

			<div id="vagas_publicas_def">		

				<div class="control-group">

					<div class="controls">

						<div id="extra_filtering"></div>

					</div>

				</div>

			</div>

			<div class="control-group">

				<?php

					$attributes = array(
					    'class' => 'control-label',
					    
					);
					echo form_label('Comentários Publicos', 'comments_publicos', $attributes);


				?>

				<div class="controls">

					<?php 

						echo form_textarea('notas_evento_publico', '', 'class="span12" style="resize: none"'); 

					?>

				</div>

			</div>	

		<?php echo form_fieldset_close() ?>

	</div>

	<div class="form-actions">

		<button type="submit" class="btn btn-primary pull-right">Salvar</button>

	</div>

<?php echo form_close() ?>					