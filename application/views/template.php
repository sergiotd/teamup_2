<!DOCTYPE html>

<html>

	<head>

		<title></title>

		<!-- bootstrap css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css') ?>">

		<!-- main css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">

		<!-- bootstrap responsive css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-responsive.css') ?>">

		<link rel="stylesheet" href="<?php echo base_url('assets/css/datepicker.css') ?>">

		<link rel="stylesheet" href="<?php echo base_url('assets/css/timepicker.css') ?>">

		<!-- jquery  -->
		<script src="<?php echo base_url('assets/js/jquery-1.7.2.js') ?>" type="text/javascript" charset="utf-8"></script>
		
		<!-- bootstrap -->
		<script src="<?php echo base_url('assets/js/bootstrap.js') ?>" type="text/javascript" charset="utf-8"></script>

		<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>" type="text/javascript" charset="utf-8"></script>

		<script src="<?php echo base_url('assets/js/bootstrap-timepicker.js') ?>" type="text/javascript" charset="utf-8"></script>

		<!-- main jquery -->
		<link rel="stylesheet/less" href="<?php echo base_url('assets/less/bootstrap.less') ?>">
		
		<link rel="stylesheet/less" href="<?php echo base_url('assets/less/datepicker.less') ?>">

		<link rel="stylesheet/less" href="<?php echo base_url('assets/less/timepicker.less') ?>">

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

		<script src="<?php echo base_url('assets/js/about_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/cart_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/contactos_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/creator_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/date_chooser_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/empresa_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/event_directions_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/info_investidores_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/info_media_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/info_patrocinadores_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/locator_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/log_in_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/log_out_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/map_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/modal_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/my_account_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/navigation_bar_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/novo_evento_activo_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/novo_evento_passivo_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/people_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/radius_buttons_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/reportar_problemas_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/search_location_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/sign_up_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/slider_container_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/sugestoes_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/user_profile_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/map_address_viewer_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/radius_buttons_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/creator_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/extra_event_filtering_ctrl.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/search_sport.js') ?>"></script>

		<script src="<?php echo base_url('assets/js/teamup.js') ?>"></script>

	</head>

	<body>

		<div class="navbar navbar-fixed-top">

			<div class="navbar-inner">

				<div class="container-fluid">

					<div class="row-fluid">

						<div class="span12">

							<div class="nav-collapse" id="navigation_bar">

								<script type="text/javascript">

									$('#navigation_bar')

										.load(

											'navigation_bar'

										);

								</script>

							</div>

						</div>

					</div>

					<div class="row-fluid" style="border-top:1px solid orange; height:70px">

						<div class="span12">

							<div class="nav-collapse" id="sub_navigation_bar">

								<script type="text/javascript">

									$('#sub_navigation_bar')

										.load(

											'navigation_bar/sub_navigation_bar',

											{}, 

											function(){

												var autoc = new google.maps.places.Autocomplete(document.getElementById('navbar_search_location_input'));


											}
										);

								</script>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="container-fluid">

			<div id="main_view"></div>

			<footer class="footer" style="border-top:1px solid whiteSmoke">

				<br>

				<div class="row-fluid main_navigation">

					<div class="span3">

						<p><?php echo anchor('contactos', 'Contactos', 'id="contactos"'); ?></p>

						<p><?php echo anchor('sugestoes', 'Sugestões', 'id="sugestoes"'); ?></p>

						<p><?php echo anchor('reportar_problemas', 'Reportar Problemas ', 'id="reportar_problemas"'); ?></p>

					</div>

					<div class="span3">

						<p><?php echo anchor('info_media', 'Informação para os Media', 'id="info_media"'); ?></p>

						<p><?php echo anchor('investidores', 'Investidores', 'id="info_investidores"'); ?></p>

						<p><?php echo anchor('patrocinadores', 'Patrocinadores', 'id="info_patrocinadores"'); ?></p>

					</div>

					<div class="span3">

						<p><?php echo anchor('empresa', 'Empresa', 'id="empresa"'); ?></p>

					</div>

					<div class="span3">

						<p><?php echo anchor('locator', 'Locator', 'id="footer_locator"'); ?></p>

						<p><?php echo anchor('creator', 'Creator', 'id="footer_creator"'); ?></p>

						<p><?php echo anchor('about', 'About', 'id="footer_about"'); ?></p>

						<p><?php echo anchor('my_account', 'My Account', 'id="footer_my_account"'); ?></p>

					</div>

				</div>

			</footer>

			<div id="modal_container"></div>

		</div>	

	</body>

</html>

