<div class="btn-group" data-toggle="buttons-radio">

  <button type="button" data-value="1000" class="btn radius_buttons active">1 Km</button>

  <button type="button" data-value="2000" class="btn radius_buttons">2 Km</button>

  <button type="button" data-value="3000" class="btn radius_buttons">3 Km</button>

  <button type="button" data-value="5000" class="btn radius_buttons">5 Km</button>

  <button type="button" data-value="10000" class="btn radius_buttons">10 Km</button>

</div>