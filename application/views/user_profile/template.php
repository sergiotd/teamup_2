<div class="row-fluid">

	<div class="span8">

		<form>

			<?php echo form_fieldset('Identificação') ?>

			<div class="control-group">

				<label class="control-label" for="input01">Sexo:</label>

				<div class="controls">

					<span class="input-xlarge uneditable-input">Masculino / Feminino</span>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label" for="input01">Idade:</label>

				<div class="controls">

					<span class="input-xlarge uneditable-input">xx</span>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label" for="input01">Email:</label>

				<div class="controls">

					<span class="input-xlarge uneditable-input">xxxxxx@xxxx.xxx</span>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label" for="input01">Telefone:</label>

				<div class="controls">

					<span class="input-xlarge uneditable-input">+00 000 00 00</span>

				</div>

			</div>

			<div class="control-group">

				<label class="control-label" for="input01">Facebook:</label>

				<div class="controls">

					<span class="input-xlarge uneditable-input"><a>link</a></span>

				</div>

			</div>

			<?php echo form_fieldset_close() ?>

			<?php echo form_fieldset('Historial') ?>

			<br>

			<table class="table table-bordered">

				<thead>

					<tr>

						<th>#</th>

						<th>Desporto</th>

						<th>Data</th>

						<th>Organizador</th>

						<th>Status</th>

					</tr>

				</thead>

				<tbody>

					<tr>

						<td>1</td>
						<td>Futebol</td>
						<td>11-06-2012</td>
						<td>Nome do Organizador</td>
						<td>Jogou</td>


					</tr>

					<tr>

						<td>1</td>
						<td>Basket</td>
						<td>11-06-2012</td>
						<td>Nome do Organizador</td>
						<td>Não Jogou</td>

					</tr>

					<tr>

						<td>1</td>
						<td>Andebol</td>
						<td>11-06-2012</td>
						<td>Nome do Organizador</td>
						<td>Convidado Não respondeu</td>


					</tr>

				</tbody>


			</table>

			<?php echo form_fieldset_close() ?>

		</form>

	</div>

	<div class="span4">

		<div class="well" style="height:200px">FOTO</div>

		<div class="row-fluid">

			<div class="span8">

				<i class="icon-star"></i>
				<i class="icon-star"></i>
				<i class="icon-star-empty"></i>
				<i class="icon-star-empty"></i>
				<i class="icon-star-empty"></i>

			</div>

			<div class="span4">

				<a href="#" class="btn btn-primary pull-right">Votar</a>

			</div>

		</div>

		<div>


		</div>

	</div>

</div>