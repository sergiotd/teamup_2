<div class="nav-collapse" id="sub_navigation_bar">

	<div class="row-fluid">

		<div class="span2">

			<?php 
				$data = array(

					'class' => 'brand'

					);

				echo anchor('', 'Team-UP', $data); 

			?>

		</div>

		<div class="span1"><p class="navbar-text">Seta:</p></div>

		<div class="span2">

			<p class="navbar-text">Sport?</p>

			<?php  

				$data = array(

					'class' => 'navbar-search pull-left',

					'style' => 'margin-top:-10px'

					);

				echo form_open('', $data);

					$data = array(

						'class' => 'search-query span12',

						'placeholder' => 'Search your Sport',

						'name' => 'search_sport',

						);

					echo form_input($data);

				echo form_close() 

			?>

		</div>

		<div class="span3">

			<p class="navbar-text">Where?</p>

			<ul class="nav nav-pills span12">

			  <li class="dropdown span12" id="menu1">

			  	<?php 

			  		$data = array(

			  			'class' => 'dropdown-toggle span12',

			  			'data-toggle' => 'dropdown',

			  			'style' => 'font-size:2em;color:orange;margin:-15px 11px 0px -70px',

			  			'content' => '' 

			  			);

			  		echo anchor('','<span>Lisbon</span><b class="caret"></b>', $data);

			  	?>

			    <ul class="dropdown-menu" style="padding: 8px 8px 0px 7px;background-color: whiteSmoke">

			    	<li style="width:325px">

			    	<?php

			    		$data = array(

			    			'class' => 'span12',

			    			'id' => 'navbar_search_location_input',

			    			'name' => 'navbar_search_location_input',

			    			'value' => ''

			    			);

			    		echo form_input($data);

			    	?>

			    	</li>

			    </ul>

			  </li>

			</ul>

		</div>

		<div class="span3">

			<p class="navbar-text">When?</p>

			<ul class="nav nav-pills span12">

			  <li class="dropdown span12" id="menu1">

			  	<?php 

			  		$data = array(

			  			'class' => 'dropdown-toggle span12',

			  			'data-toggle' => 'dropdown',

			  			'style' => 'font-size:2em;color:orange;margin:-15px 11px 0px -70px',

			  			'content' => '' 

			  			);

			  		echo anchor('','<span>This Week</span><b class="caret"></b>', $data);

			  	?>

			    <ul class="dropdown-menu" style="padding: 8px 8px 0px 7px;background-color: whiteSmoke">

			    	<li style="width:325px">


			    	</li>

			    </ul>

			  </li>

			</ul>

		</div>

		<div class="span1">

			<button type="button" class="btn">Search</button>

		</div>

	</div>

</div>

<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

	<span class="icon-bar"></span>

	<span class="icon-bar"></span>

	<span class="icon-bar"></span>

</a>