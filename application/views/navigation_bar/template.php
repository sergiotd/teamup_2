<div class="row-fluid">

	<div class="span3">

		<ul class="nav main_navigation">

			<?php if (isset($full_name)): ?>

				<li id="navigation_welcome_message">

					<p class="navbar-text">Welcome Sérgio Duarte</p>

				</li>

			<?php endif ?>

		</ul>

	</div>

	<div class="span4">

		<ul class="nav main_navigation">

			<?php if ($logged_in === TRUE): ?>

				<li>

					<a 

						href="<?php echo site_url('log_out') ?>"

						id="log_out"

					>Log out</a>

				</li>	

			<?php else: ?>

				<li>

					<a 

						href="<?php echo site_url('log_in') ?>"

						id="log_in"

					>Log in</a>

				</li>	

				<li>

					<a 

						href="<?php echo site_url('sign up') ?>"

						id="sign_up"

					>Sign up</a>

				</li>	
			
			<?php endif ?>

			<li>

				<a 

					href="<?php echo site_url('about') ?>"

					id="about"

				>About us</a>

			</li>

			<li>

				<a 

					href="<?php echo site_url('faq') ?>"

					id="faq"

				>FAQ</a>

			</li>

			<li>

				<a 

					href="<?php echo site_url('help') ?>"

					id="help"

				>Help</a>

			</li>

		</ul>

	</div>

	<div class="span3">

		<ul class="nav main_navigation">

			<li>

				<p class="navbar-text">Follow us on:</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/social_networks/facebook_16.png') ?>">

				</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/social_networks/youtube_16.png') ?>">

				</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/social_networks/twitter_16.png') ?>">

				</p>

			</li>	

		</ul>

	</div>

	<div class="span2">

		<ul class="nav main_navigation">

			<li>

				<p class="navbar-text">Language:</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/countries/Portugal-Flag-16.png') ?>">

				</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/countries/United-Kingdom-Flag-16.png') ?>">

				</p>

			</li>

			<li>

				<p class="navbar-text" style="margin-left:4px">

					<img src="<?php echo base_url('assets/img/countries/France-Flag-16.png') ?>">

				</p>

			</li>

		</ul>

	</div>

</div>