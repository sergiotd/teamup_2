<div class="row-fluid">

	<div class="span2">

		<div class="input-append bootstrap-timepicker-component">

		    <input type="text" class="span8 timepicker start input-small" readonly>

			<i class="icon-time"></i>

		</div>

	</div>

	<div class="span2">

		<div class="input-append bootstrap-timepicker-component">

		    <input type="text" class="span8 timepicker end input-small" readonly>

			<i class="icon-time"></i>

		</div>

	</div>

	<div class="span2">

		<input class="timepicker-duration span8" type="text" readonly>

	</div>

	<div class="span3">

		<?php echo form_checkbox('anytime', 'anytime', FALSE); ?> Anytime

	</div>

</div>

<script type="text/javascript">

	$('.timepicker').timepicker({

	    template : 'dropdown',

	    showInputs: false,

	    showMeridian: false

	});

</script>