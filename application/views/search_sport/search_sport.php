<div class="row-fluid">

	<div class="span9">

		<input 

			name="sport" 

			id="search_sport_input" 

			type="text"

			<?php if (isset($sport_id) && isset($sport)): ?>

				data-sport-id = "<?php echo $sport_id ?>"

				value="<?php echo $sport ?>"
				
			<?php endif ?>

			class="span12" 

			placeholder="Type the name of the sport to search for">

	</div>

	<div class="span3">

		<button id="search_sport_button" type="button" class="btn span12">Search</button>

	</div>

</div>

<div id="sport_chooser"></div>
