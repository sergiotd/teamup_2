<div id="myCarousel" class="carousel slide columns">

	<!-- Carousel items -->

	<div class="carousel-inner">

		<div class="active item">

			<img src="<?php echo base_url('assets/img/apagar/01.jpeg') ?>" width="100%" height="250px">

		</div>

		<div class="item">

			<img src="<?php echo base_url('assets/img/apagar/02.jpeg') ?>" width="100%" height="250px">

		</div>

		<div class="item">

			<img src="<?php echo base_url('assets/img/apagar/03.jpeg') ?>" width="100%" height="250px">

		</div>

	</div>

	<!-- Carousel nav -->

	<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>

	<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>

</div>