<div class="row-fluid">

	<div class="span2" id="pub_left"></div>

	<div class="span8">

		<form class="form-horizontal">

		<?php echo form_fieldset('Tipo de Novo') ?>

			<div class="control-group">

				<div class="controls">

					<?php echo form_dropdown('tipo_de_evento', $tipo_de_evento, '0', 'class="span6"'); ?>

				</div>

			</div>

		<?php echo form_fieldset_close() ?>

		</form>

		<div id="novo_evento_container"></div>

	</div>

	<div class="span2" id="pub_right"></div>

</div>