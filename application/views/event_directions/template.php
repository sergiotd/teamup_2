<?php echo img($static_map) ?>

<div style="padding-top:10px" class="row-fluid">

	<div class="span10">

		<?php echo form_input('location_start', '', 'class="span12" placeholder="Introduza o local de Partida" id="location_start"'); ?>

	</div>

	<div class="span2">

		<button 
		class="btn span12" 

		type="button" 

		id="get_directions_button" 

		data-dest-lat="<?php echo $lat ?>"

		data-dest-lng="<?php echo $lng ?>"><i class="icon-ok"></i></button>


	</div>


</div>

<div id="directions_panel"></div>	