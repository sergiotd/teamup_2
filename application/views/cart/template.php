<div class="well-white">

	<table class="table table-condensed">

		<thead>

			<tr>

				<th style="width:4%; max-width:4%; "></th>

				<th>Desporto</th>

				<th>Organizador</th>

				<th>Lat</th>

				<th>Lng</th>

				<th>Morada</th>

				<th>Cidade</th>

				<th>Distrito</th>

				<th>Pais</th>

				<th>Inicio</th>

				<th>Fim</th>

				<th>Status</th>	

			</tr>

		</thead>

		<tbody>

			<?php foreach ($query->result_array() as $row): ?>
			
				<?php $this->load->view('cart/single_row', array('row' => $row)) ?>

			<?php endforeach ?>

		</tbody>

	</table>

</div>