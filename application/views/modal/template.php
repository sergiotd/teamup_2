<div class="modal hide" id="myModal">

	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal">×</button>

		<h3 id="modal_header"></h3>

	</div>

	<div class="modal-body"></div>

	<div class="modal-footer">

		<div id="modal_functions"></div>

		<br>

		<div class="row-fluid">

			<div class="span6" id="modal_pub_left"></div>

			<div class="span6" id="modal_pub_right"></div>

		</div>

	</div>

</div>