<div class="row-fluid">

	<div class="span9">

		<input 

			id="search_location_input" 

			name="location"

			type="text" 

			class="span12"

			placeholder="Please type your location">

	</div>

	<div class="span3">

		<button 

			id="search_location_button" 

			type="button" 

			class="btn span12 <?php echo $search_button_html_class ?>"

		>Search</button>

	</div>

	<input name="location_lat" type="hidden">

	<input name="location_lng" type="hidden">

</div>