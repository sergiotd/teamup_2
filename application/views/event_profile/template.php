<ul class="nav nav-tabs">

	<li class="active"><a href="#lA" data-toggle="tab">Perfil</a></li>

	<li class=""><a href="#lE" data-toggle="tab">Evento Activo</a></li>

	<li class=""><a href="#lB" data-toggle="tab">Localização</a></li>

	<li class=""><a href="#lC" data-toggle="tab">Comentários</a></li>

	<li class=""><a href="#lD" data-toggle="tab">Historial</a></li>

</ul>

<div class="row-fluid">

	<div class="span12">

		<div class="tab-content">

			<div class="tab-pane active" id="lA">

				<?php $this->load->view('event_profile/main_info') ?>

			</div>

			<div class="tab-pane" id="lB">

				<div id="event_directions"></div>

			</div>

			<div class="tab-pane" id="lE">

				<?php $this->load->view('active_event/template.php') ?>

			</div>

			<div class="tab-pane" id="lC">

				<div id="event_comments">

					<?php $this->load->view('comments/template') ?>

				</div>

			</div>

			<div class="tab-pane" id="lD">

				<?php $this->load->view('historial/template') ?>

			</div>

		</div>

	</div>

</div>