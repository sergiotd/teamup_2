<label class="control-label" for="input01">Organizador:</label>

<div class="row-fluid">

	<div class="span8">

		<span class="input-xlarge uneditable-input">

			Nome do Organizador

		</span>

	</div>

	<div class="span4">

		<div class="pull-right">

			<?php $this->load->view('star_ranking/template') ?>

		</div>
			
	</div>

</div>

<label class="control-label" for="input01">Desporto:</label>

<div class="row-fluid">

	<div class="span8">

		<span class="input-xlarge uneditable-input">

			Futebol 7

		</span>

	</div>

	<div class="span4"></div>

</div>

<?php echo form_fieldset('Notas Adicionais') ?>

<br>

<?php echo form_textarea('name', 'NOTAS ADICIONAIS - Equipamento a levar, levar dinheiro etc...', 'disabled class="span12"'); ?>

<br>

<?php echo form_fieldset('Media') ?>

<?php $this->load->view('media_viewer/template') ?>
