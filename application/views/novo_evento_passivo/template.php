<div id="form_errors"></div>

<?php echo form_open('creator/novo_evento_passivo_submit', 'class="form-horizontal" id="form_novo_evento_passivo"'); ?>

	<?php echo form_fieldset('What') ?>

		<div class="control-group">

			<div class="controls">

				<div id="search_sport"></div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('When') ?>

		<div class="control-group">

			<div class="controls">

				<div id="date_chooser"></div>

			</div>

		</div>

		<div class="control-group">

			<div class="controls">

				<div id="time_chooser"></div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('Where') ?>

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="search_location"></div>

			</div>

		</div>

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="radius_buttons"></div>

			</div>

		</div>

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="map_canvas"></div>

			</div>

		</div>		

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="map_address_viewer"></div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<?php echo form_fieldset('Extra') ?>

		<div class="control-group">

			<div class="controls">

				<div class="span12" id="extra_filtering"></div>

			</div>

		</div>

		<div class="control-group">

			<label class="control-label">Comentários</label>

			<div class="controls">

				<div class="span12" id="comments">

					<?php echo form_textarea('comments', '', 'class="span12" style="resize: none"'); ?>

				</div>

			</div>

		</div>

	<?php echo form_fieldset_close() ?>

	<div class="form-actions">

		<button type="submit" class="btn btn-primary pull-right">Salvar</button>

	</div>

<?php echo form_close() ?>