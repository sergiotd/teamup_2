<div class="row-fluid">

	<div class="span10">

		<div id="where_container"></div>

		<div id="what_container"></div>

		<div id="when_container"></div>

		<div class="row-fluid">

			<div class="span4">

				<button 

					class="btn span12 btn-large btn-danger" 

					type="button"

					id="main_search_locator_button"

					>

					Search</button>


			</div>

			<div class="span8">

				<div id="extra_filtering"></div>

			</div>
			
		</div>

		<div class="row-fluid">

			<div class="span4 well well-small">

				<div id="people" class="well-white" style="height:411px;max-height:411px;"></div>

			</div>

			<div class="span8">

				<div id="map_canvas"></div>

				<br>

				<div id="top_rated_events_buttons"></div>

			</div>

		</div>

	</div>

	<div class="span2" id="pub_right"></div>

</div>

<div class="well well-small" id="cart"></div>