<div class="row-fluid slider_main_row <?php echo $slider_bullet_color ?>">
		
	<div class="span1 slider_bullet_div">

		<div class="square_bullet <?php echo $slider_bullet_color ?>"></div>

	</div>	

	<div class="span10">

		<div class="slider_container_value <?php echo $slider_bullet_color ?>">

			<span><?php echo $slider_title ?></span>

			<span class="slider_subtitle"><?php echo $slider_sub_title ?></span>

		</div>

	</div>	

	<div class="span1">

		<button 

			type="button" 

			class="btn btn-mini pull-right open_collapse" 

			data-toggle="collapse" 

			data-target="<?php echo '#' . $toggle_div_id ?>"

			style="padding: 1px 1px 2px 2px;border-radius: 0px 0px 2px 2px;border-color: white;margin-right: 1px;"

			>

			<i class="icon-chevron-<?php echo $open_icon_direction ?>"></i>

		</button>

	</div>	

</div>

<div class="row-fluid" style="background-color: whitesmoke;">

	<div id="<?php echo $toggle_div_id ?>" class="collapse <?php echo $show_main ?>" style="margin:5px"></div>
	
</div>	

