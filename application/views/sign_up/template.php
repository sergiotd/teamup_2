<?php echo validation_errors(); ?>

<form action="<?php echo site_url('sign_up/submit') ?>" class="form-horizontal" method="POST" id="sign_up_form">

	<label>First Name</label>
	
	<input type="text" name="first_name" value="<?php echo set_value('first_name') ?>" placeholder="">

	<label>Last Name</label>
	
	<input type="text" name="last_name" value="<?php echo set_value('last_name') ?>" placeholder="">

	<label>Avatar</label>
	
	<input type="text" name="avatar" value="<?php echo set_value('avatar') ?>" placeholder="">

	<label>Email</label>

	<input type="text" name="email" value="<?php echo set_value('email') ?>" placeholder="">

	<label>Default Country</label>

	<?php echo form_dropdown('default_country', $countries, ''); ?>

	<label>Default Language</label>

	<?php echo form_dropdown('default_language', $languages, ''); ?>

	<label>Password</label>

	<label>Faixa Etária</label>

	<?php echo form_dropdown('faixa_etaria', $faixas_etarias, ''); ?>

	<label>Password</label>
	
	<input type="password" name="password" value="" placeholder="">

	<label>Password Confirmation</label>
	
	<input type="password" name="password_conf" value="" placeholder="">

</form>