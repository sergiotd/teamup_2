<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Date_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}

	function prep_date($date = array())
	{

		$prepped_date = $date['date'] . ' ' . $date['time'].':00';

		$timestamp = strtotime($prepped_date);

		return date("Y-m-d H:i:s", $timestamp);

	}

	function subtract_date($start_date, $time)
	{

		$timestamp = $start_date - strtotime($time);

		return date("Y-m-d H:i:s", $timestamp);


	}

}