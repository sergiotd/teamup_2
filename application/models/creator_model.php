<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creator_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}

	function get_tipos_de_participacao()
	{

		$values = $this->db->get('tipos_de_participacao')->result_array();

		$dropdown_values = array_to_dropdown($values, 'id', 'tipo');

		$dropdown_values[0] = 'Escolha';

		return $dropdown_values;

	}	

}
