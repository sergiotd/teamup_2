<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_In_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}
		
	function check_credentials($table_name = '', $data = array())
	{

		if ($this->get_rows($table_name, $data)->num_rows() == 1) {

			return TRUE;

		}else{

			return FALSE;

		}
	
	}

	function get_full_name($user_id)
	{

		$this->db->select('first_name, last_name');

		$this->db->from('users');

		$this->db->where('id', $user_id);

		$result = $this->db->get()->result_array();

		$full_name = $result[0]['first_name'] . ' ' . $result[0]['last_name'];

		return $full_name;


	}


}