<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}

	function get_available_users($sport = '', $map_bounds = '')
	{

		$bounds = explode(',', $map_bounds);

		$lat_lo = $bounds[0];

		$lng_lo = $bounds[1];

		$lat_hi = $bounds[2];

		$lng_hi = $bounds[3];

		$select = array(

			'locations_has_users.users_id',

			'users.avatar',

			'locations_has_users.radius'

			);

		$this->db->select($select);

		$this->db->from('locations_has_users');

		$this->db->join('locations', 'locations_has_users.locations_id = locations.id');

		$this->db->join('sports', 'locations_has_users.sports_id = sports.id');

		$this->db->join('users', 'locations_has_users.users_id = users.id');

		$this->db->where('sports.sport', $sport);

		$this->db->where('locations.lat < ', $lat_hi);

		$this->db->where('locations.lat > ', $lat_lo);

		$this->db->where('locations.lng < ', $lng_hi);

		$this->db->where('locations.lng > ', $lng_lo);

		return $this->db->get()->result_array();
	}

}