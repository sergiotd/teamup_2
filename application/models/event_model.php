<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}

	function get_profile_info($id = '')
	{

		$select = array(

			'CONCAT("users.first_name", " ", "users.last_name") AS organizer',

			'sports.sport',

			'dates_times.date_time',

			'comentario_publico'

			);

		$this->db->select($select);

		$this->db->from('events');

		$this->db->join('users', 'events.organizer_id = users.id');

		$this->db->join('events_has_dates_times', 'events.id = events_has_dates_times.events_id');

		$this->db->join('dates_times', 'events_has_dates_times.start_time_id = dates_times.id');

		$this->db->join('sports', 'events.sports_id = sports.id');

		$this->db->where('events.id', '3');

		die(var_dump($this->db->get()->result_array()));

		return array_values($this->db->get()->result_array());


	}

}
