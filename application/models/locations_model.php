<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locations_Model extends MY_Model { 

	var $lat;
	var $lng;
	var $street;
	var $door_number;
	var $parish;
	var $city;
	var $country;

	public function __construct($data = array())
	{
	   parent::__construct();

	   	if (! empty($data)) {

			foreach ($data as $key => $value) {

				$name = 'set_'.$key;
				
				$this->$name($value);

			}

	   	}

	}

	function set_lat($value = '')
	{

		$this->lat = $value;

	}

	function set_lng($value = '')
	{

		$this->lng = $value;

	}

	function set_street($value)
	{

		// street exists?
		$this->db->select('id');

		$this->db->from('streets');

		$this->db->where('street', $value);

		$query = $this->db->get()->result();

		if ($query->num_rows() == 0) {

			$this->db->insert('streets', array('street' => $value))->insert_id()

		}		

	}

	function 




}