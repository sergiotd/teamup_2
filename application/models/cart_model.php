<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();

	}
		
	function get_all_favorites($user_id)
	{

		$select = array(

			'events.id',

			'sports.sport',

			'events.organizer_id',

			'locations.lat',

			'locations.lng',

			'locations.morada',

			'locations.cidade',

			'locations.distrito',

			'countries.country',

			'events.start',

			'events.end',

			'candidaturas_status.status'

			);

		$this->db->select($select);

		$this->db->from('favoritos');

		$this->db->join('users', 'favoritos.users_id = users.id');

		$this->db->join('events', 'favoritos.events_id = events.id');

		$this->db->join('sports', 'events.sports_id = sports.id');

		$this->db->join('locations', 'events.locations_id = locations.id');

		$this->db->join('countries', 'locations.countries_id = countries.id');

		$this->db->join('candidaturas', 'events.id = candidaturas.events_id', 'left');

		$this->db->join('candidaturas_status', 'candidaturas.candidaturas_status_id = candidaturas_status.id', 'left');

		$this->db->where('users.id', $user_id);

		return $this->db->get();


	}


}