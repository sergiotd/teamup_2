<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_Profile extends MY_Controller {

	var $data = array();

   var $eventid = NULL;

   public function __construct()
   {
      parent::__construct();

      $this->eventid = $this->input->post('id');

   }

   public function index()
   {

      $this->data['id'] = $this->eventid;   

		$this->load->view('event_profile/template', $this->data);

   }

   public function marker()
   {

      $this->load->model('event_model');

      $query = $this->event_model->get_profile_info($this->eventid);

      die(var_dump($query));


		$this->data['id'] = $this->eventid;



      $output['modal_header'] = '20 de Julho de 2012 (Têrça-Feira)  <span class="pull-right" style="margin-right:10px">14:00 - 15:30</span>';

		$output['modal_body'] = $this->load->view('event_profile/template', $this->data, TRUE);

		$output['modal_functions'] = $this->load->view('event_profile/marker_functions', $this->data, TRUE);	

		echo json_encode($output);

   }

   public function cart()
   {

      $this->data['id'] = $this->eventid;

      $output['modal_header'] = '20 de Julho de 2012 (Têrça-Feira)  <span class="pull-right" style="margin-right:10px">14:00 - 15:30</span>';

      $output['modal_body'] = $this->load->view('event_profile/template', $this->data, TRUE);

      $output['modal_functions'] = $this->load->view('event_profile/cart_functions', $this->data, TRUE);  

      
      echo json_encode($output);

   }

}

/* End of file Event_Profile.php */
/* Location: ./application/controllers/Event_Profile.php */