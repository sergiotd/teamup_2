<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('about/template_about', $this->data);

   }

}

/* End of file about.php */
/* Location: ./application/controllers/about.php */