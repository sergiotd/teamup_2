<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_account extends MY_Controller {

	var $data = array();

   public function index()
   {

		$this->load->view('my_account/template_my_account', $this->data);

   }

}

/* End of file my_account.php */
/* Location: ./application/controllers/my_account.php */