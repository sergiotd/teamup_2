<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_In extends MY_Controller {

	var $data = array();

	public function __construct()
	{

		parent::__construct();

		$this->load->model('log_in_model');

	}

   public function index()
   {

   		$output['modal_header']    = 'LOG IN';

   		$output['modal_body']      = $this->load->view('log_in/template', $this->data, TRUE);

   		$output['modal_functions'] = $this->load->view('log_in/functions', $this->data, TRUE);

   		echo json_encode($output);

   }

   public function submit()
   {

		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_login_check|callback_set_logged_in');

		if ($this->form_validation->run() === FALSE)
		{

			$this->load->view('log_in/template', $this->data);

		}else{

			echo 'true';

		}

   }

   public function login_check()
   {

		$data = array(

			'email' => $this->input->post('email'),

			'password' => md5($this->input->post('password'))

			);

		if ($this->log_in_model->check_credentials('users', $data)){

			return TRUE;

		} else {

			$this->form_validation->set_message('login_check', 'Email and Password Pair failed');

	   		return FALSE;

		}

	}

	public function set_logged_in()
	{

		$this->load->helper('date');

		$datestring = "%Y-%m-%d %h:%i:%s";

		$time = time();
		
		$data = array(

			'last_login' =>mdate($datestring, $time)

			);

		$update = array(

			'email' => $this->input->post('email')

			);

		if ($this->log_in_model->update('users', $data, $update)) {

			$q = $this->log_in_model->get_rows('users', array('email' => $this->input->post('email')));

			$q = $q->result_array();

			$user_id = $q[0]['id'];

			$name = $q[0]['first_name'] . ' ' . $q[0]['last_name'];

			$language_id = $q[0]['default_language_id'];

			// $user_id = $this->log_in_model->get_id('users', $update);

			// $name = $this->log_in_model->get_full_name($user_id);

			// $q = $this->log_in_model->get_rows('users', array('id' => $user_id));

			// $q_temp = $q->result_array();

			// $language_id = $q_temp[0]['language_id'];

			$newdata = array(

                   'logged_in' => TRUE,

                   'user_id' => $user_id,

                   'full_name' => $name,

                   'language_id' => $language_id

               	);

			$this->session->set_userdata($newdata);

			return TRUE;

		} else {

			$this->form_validation->set_message('set_logged_in', 'LogGed IN Problem');

			return FALSE;

		}

	}

}

/* End of file Log_In.php */
/* Location: ./application/controllers/Log_In.php */