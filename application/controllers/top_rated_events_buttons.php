<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Top_Rated_Events_Buttons extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('top_rated_events_buttons/template', $this->data);

   }

}

/* End of file Top_Rated_Events_Buttons.php */
/* Location: ./application/controllers/Top_Rated_Events_Buttons.php */