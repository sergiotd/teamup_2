<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider_Container extends MY_Controller {

	var $data = array();

   public function index()
   {

      $this->data['toggle_div_id'] = $this->input->post('toggle_div_id');

      $this->data['slider_title'] = $this->input->post('slider_title');

      $this->data['slider_sub_title'] = $this->input->post('slider_sub_title');

      $this->data['slider_bullet_color'] = $this->input->post('slider_bullet_color');

      $this->slider_open_close();

      $this->load->view('slider_container/template', $this->data);

   }

   public function slider_open_close(){

      if ($this->input->post('open') == 'true') {

         $this->data['show_main'] = 'in';

         $this->data['open_icon_direction'] = 'up';



      } else {

         $this->data['show_main'] = '';

         $this->data['open_icon_direction'] = 'down';

      }
      


   }

}

/* End of file Slider_Container.php */
/* Location: ./application/controllers/Slider_Container.php */