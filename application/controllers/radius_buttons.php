<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Radius_Buttons extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('radius_buttons/template', $this->data);

   }

}

/* End of file Radius_Buttons.php */
/* Location: ./application/controllers/Radius_Buttons.php */