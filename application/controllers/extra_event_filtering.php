<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Extra_Event_Filtering extends MY_Controller {

	var $data = array();

   public function index()
   {

   		
   		$this->load->model('extra_event_filtering_model');

   		$this->data['faixas_etarias'] = $this->extra_event_filtering_model->get_faixas_etarias_list();

   		$this->data['social_networks'] = $this->extra_event_filtering_model->get_social_networks_list();

   		$this->load->view('extra_event_filtering/template', $this->data);

   }

}

/* End of file Extra_Event_Filtering.php */
/* Location: ./application/controllers/Extra_Event_Filtering.php */