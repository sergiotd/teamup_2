<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invite_Selected_Passive extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$output['modal_header'] = 'Convidar';	

   		$output['modal_body'] = $this->load->view('invite_selected_passive/template', $this->data, TRUE);	

   		$output['modal_functions'] = $this->load->view('invite_selected_passive/modal_functions', $this->data, TRUE);	

   		echo json_encode($output);
   		
	}

}

/* End of file Invite_Selected_Passive.php */
/* Location: ./application/controllers/Invite_Selected_Passive.php */