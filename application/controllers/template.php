<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation_Bar extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('navigation_bar/template', $this->data);

   }

}

/* End of file Navigation_Bar.php */
/* Location: ./application/controllers/Navigation_Bar.php */