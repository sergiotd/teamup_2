<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	var $data = array();

	public function initialize()
	{

		$this->load->view('template', $this->data);

		

	}

	public function index()
	{

		$this->load->view('home/template_home', $this->data);

	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */