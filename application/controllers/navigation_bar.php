<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Navigation_Bar extends MY_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->data['logged_in'] = $this->session->userdata('logged_in');

	}

	public function index()
	{

		if ($this->data['logged_in'] === TRUE) {

			$this->data['full_name'] = $this->session->userdata('full_name');

		}
		
		$this->load->view('navigation_bar/template', $this->data);

	}

	public function sub_navigation_bar()
	{

		$this->load->view('navigation_bar/sub_navigation_bar', $this->data);


	}

}

/* End of file Navigation_Bar.php */
/* Location: ./application/controllers/Navigation_Bar.php */