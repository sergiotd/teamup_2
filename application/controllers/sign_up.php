<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sign_Up extends MY_Controller {

	var $data = array();

	var $sign_up = array();

	public function index()
	{

   		$output['modal_header']    = 'Sign UP';

   		$this->load->model('sign_up_model');

   		$this->data['countries'] = $this->sign_up_model->get_country_list();

   		$this->data['languages'] = $this->sign_up_model->get_languages_list();

   		$this->data['faixas_etarias'] = $this->sign_up_model->get_faixas_etarias_list();

   		$output['modal_body']      = $this->load->view('sign_up/template', $this->data, TRUE);

   		$output['modal_functions'] = $this->load->view('sign_up/functions', $this->data, TRUE);

   		echo json_encode($output);

	}

	public function submit()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[12]');

		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[12]');

		$this->form_validation->set_rules('avatar', 'avatar', 'trim|required|max_length[25]');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password_conf]|md5');

		$this->form_validation->set_rules('password_conf', 'Password Confirmation', 'trim|required');

		$this->form_validation->set_rules('faixa_etaria', 'Faixa Etaria', '');

		if ($this->form_validation->run() == FALSE)
		{

			$this->load->view('sign_up/template', $this->data);

		}else{

			$this->load->model('sign_up_model');

			$result = $this->sign_up_model->insert('users', $this->prep_data());

			if ($result != FALSE) {

				echo 'true';

			}else{

				echo 'false';

			}

		}

	}

	public function prep_data()
	{

		$data = array(

			'first_name' => $this->input->post('first_name'),

			'last_name'  => $this->input->post('last_name'),

			'email'      => $this->input->post('email'),

			'default_country_id'      => $this->input->post('default_country_id'),

			'default_language_id'      => $this->input->post('default_language_id'),

			'password'   => $this->input->post('password'),

			'avatar'     => $this->input->post('avatar'),

			'faixas_etarias_id' => $this->input->post('faixa_etaria')

			);

		return $data;

	}

}

/* End of file sign_up.php */
/* Location: ./application/controllers/sign_up.php */