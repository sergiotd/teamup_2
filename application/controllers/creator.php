<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creator extends MY_Controller {

	var $data = array();

	var $sport_id = NULL;

	var $street_id = NULL;

	var $door_number_id = NULL;

	var $city_id = NULL;

	var $parish_id = NULL;

	var $country_id = NULL;

	var $location_id = NULL;

	var $start_date_id = NULL;

	var $end_date_id = NULL;

	var $public_in_date_id = NULL;

	var $venue_id = NULL;

	var $event_id = NULL;

	var $num_vagas_publicas = NULL;

	var $num_vagas_privadas = NULL;

	var $public_in_date = NULL;

	public function index()
	{

		$this->data['tipo_de_evento'] = array(

				'0' => '-',

				'activo' => 'Organizador',

				'passivo' => 'Jogador'

			);

		$this->load->view('creator/template_creator', $this->data);

	}

	function novo_evento_activo()
	{

		$this->load->model('creator_model');

		$this->data['tipos_de_participacao'] = $this->creator_model->get_tipos_de_participacao();

		$this->load->view('novo_evento_activo/template', $this->data);

	}

	function novo_evento_activo_submit()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('sport', 'sport', 'trim|required');

		$this->form_validation->set_rules('date', 'date', 'trim|required');

		$this->form_validation->set_rules('start_time', 'start_time', 'trim|required');

		$this->form_validation->set_rules('end_time', 'end_time', 'trim|required');

		$this->form_validation->set_rules('street', 'street', 'trim|required');

		$this->form_validation->set_rules('door_number', 'door_number', 'trim|required');

		$this->form_validation->set_rules('parish', 'parish', 'trim|required');

		$this->form_validation->set_rules('city', 'city', 'trim|required');

		$this->form_validation->set_rules('country', 'country', 'trim|required');

		$this->form_validation->set_rules('lat', 'lat', 'trim|required');

		$this->form_validation->set_rules('lng', 'lng', 'trim|required');

		$this->form_validation->set_rules('venue_name', 'Venue Name', 'trim|required');

		$this->form_validation->set_rules('max_jogadores', 'Maximo de Jogadores', 'trim|required');

		$this->form_validation->set_rules('tipo_de_participacao', 'Tipo de Participacao', 'callback_tipo_de_participacao');

		$this->form_validation->set_rules('comments_privado', 'Comentarios Privado', 'trim');

		$this->form_validation->set_rules('num_vagas_publicas', 'Checkbox Vagas Publicas Automaticas', '');

		$this->form_validation->set_rules('comments_publico', 'Comentários Publicos', 'callback_insert_activo_event');

		if ($this->form_validation->run() == FALSE)
		{

			$output = $this->form_validation->error_array();

			$output['result'] = FALSE;

			echo json_encode($output);

		}else{

			$output = array();

			$output['result'] = TRUE;

			echo json_encode($output);

		}

	}

	function tipo_de_participacao()
	{

		if ($this->input->post('tipo_de_participacao') != '0') {

			return TRUE;

		} else {

			return FALSE;

		}
		
	}



	function insert_activo_event()
	{

		$this->db->trans_start();

			$this->insert_sport();

			$this->insert_street();

			$this->insert_door_number();	

			$this->insert_city();

			$this->insert_parish();

			$this->insert_country();

			$this->insert_location();

			$this->insert_dates();

			$this->insert_venue();

			$this->insert_event();

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{

			return FALSE;

		}else{

			return TRUE;

		}


	}

	function novo_evento_activo_submit_success()
	{

		$this->data['modal_header'] = "Sucesso";

		$this->data['modal_body'] = "O Evento Activo foi inserido com sucesso";

		$this->data['modal_functions'] = $this->load->view('novo_evento_activo/success_functions', $this->data, TRUE);

		echo json_encode($this->data);


	}

	function novo_evento_passivo()
	{

		$this->load->view('novo_evento_passivo/template', $this->data);

	}

	function novo_evento_passivo_submit()
	{

		$this->load->library('form_validation');

		$this->form_validation->set_rules('sport', 'sport', 'trim|required');

		$this->form_validation->set_rules('date', 'date', 'trim|required');

		$this->form_validation->set_rules('start_time', 'start_time', 'trim|required');

		$this->form_validation->set_rules('end_time', 'end_time', 'trim|required');

		$this->form_validation->set_rules('radius', 'radius', 'trim|required');

		$this->form_validation->set_rules('street', 'street', 'trim|required');

		$this->form_validation->set_rules('door_number', 'door_number', 'trim|required');

		$this->form_validation->set_rules('parish', 'parish', 'trim|required');

		$this->form_validation->set_rules('city', 'city', 'trim|required');

		$this->form_validation->set_rules('country', 'country', 'trim|required');

		$this->form_validation->set_rules('lat', 'lat', 'trim|required');

		$this->form_validation->set_rules('lng', 'lng', 'trim|required|prep_data|callback_insert_passive_event');

		if ($this->form_validation->run() == FALSE)
		{

			$output = $this->form_validation->error_array();

			$output['result'] = FALSE;

			echo json_encode($output);

		}else{

			$output = array();

			$output['result'] = TRUE;

			echo json_encode($output);

		}

	}

	function insert_passive_event()
	{

		$this->db->trans_start();

		$this->insert_sport();

		$this->insert_street();

		$this->insert_door_number();	

		$this->insert_city();

		$this->insert_parish();

		$this->insert_country();

		$this->insert_location();

		$this->insert_dates();

		// Inserir Location has Users
		$this->load->model('locations_has_users_model');

		$data = array(

			'locations_id' => $this->location_id,

			'users_id' => $this->session->userdata('user_id'),
			
			'sports_id' => $this->sport_id,

			'radius' => $this->input->post('radius'),

			'sexo' => $this->input->post('sexo'),

			'comments' => $this->input->post('comments')

			);

		$locations_has_users_id = $this->locations_has_users_model->insert('locations_has_users', $data);


		// Inserir Faixas Etarias
		if ($this->input->post('faixas_etarias') != '') {

			$this->load->model('faixas_etarias_has_locations_has_users_model');

			$data = explode(',', $this->input->post('faixas_etarias'));

			foreach ($data as $value) {

				$temp_array = array(

					'faixas_etarias_id' => $value,

					'locations_has_users_id' => $locations_has_users_id


					);

				$this->faixas_etarias_has_locations_has_users_model->insert('faixas_etarias_has_locations_has_users', $temp_array);
			}

		}
		
		if ($this->input->post('amigos') != '') {

			// Inserir Amigos
			$this->load->model('locations_has_users_has_social_networks_model');

			$data = explode(',', $this->input->post('amigos'));

			foreach ($data as $value) {

				$temp_array = array(

					'locations_has_users_id' => $locations_has_users_id,

					'social_networks_id' => $value

					);

				$this->locations_has_users_has_social_networks_model->insert('locations_has_users_has_social_networks', $temp_array);
				
			}

		}
		

		// Inserir Location has users has dates times
		$this->load->model('locations_has_users_has_dates_times_model');

		$data = array(

			'start_dates_times_id' => $this->start_date_id,

			'end_dates_times_id' => $this->end_date_id,

			'locations_has_users_id' => $locations_has_users_id

			);

		$location_has_users_has_dates_times_id = $this->locations_has_users_has_dates_times_model->insert_if_not_exists('locations_has_users_has_dates_times', $data);

		if ($location_has_users_has_dates_times_id === FALSE) {

			$this->db->trans_rollback();

			$this->form_validation->set_message('insert_passive_event', 'Problema com a inserção na Base de Dados');

			return FALSE;

		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{

			return FALSE;

		}else{

			return TRUE;

		}

	}

	function novo_evento_passivo_submit_success()
	{

		$this->data['modal_header'] = "Sucesso";

		$this->data['modal_body'] = "O Evento passivo foi inserido com sucesso";

		$this->data['modal_functions'] = $this->load->view('novo_evento_passivo/success_functions', $this->data, TRUE);

		echo json_encode($this->data);
	
	}

	function insert_sport()
	{

		$this->load->model('sport_model');

		$data = array(

			'sport' => $this->input->post('sport'),

			'languages_id' => 6
			
			);

		$this->sport_id = $this->sport_model->insert_if_not_exists('sports', $data);

	}

	function insert_street()
	{

		$this->load->model('street_model');

		$data = array(

			'street' => $this->input->post('street')

			);

		$this->street_id = $this->street_model->insert_if_not_exists('streets', $data);		

	}

	function insert_door_number()
	{

		$this->load->model('door_number_model');

		if ($this->input->post('door_number') == '') {

			$number = 'n.a';

		} else {

			$number = $this->input->post('door_number');

		}
		

		$data = array(

			'number' => $number

			);

		$this->door_number_id = $this->door_number_model->insert_if_not_exists('doors_number', $data);

	}

	function insert_city()
	{

		$this->load->model('city_model');

		$data = array(

			'city' => $this->input->post('city')

			);

		$this->city_id = $this->city_model->insert_if_not_exists('cities', $data);

	}

	function insert_parish()
	{

		$this->load->model('parish_model');

		$data = array(

			'parish' => $this->input->post('parish')

			);

		$this->parish_id = $this->parish_model->insert_if_not_exists('parishes', $data);

	}

	function insert_country()
	{

		$this->load->model('country_model');

		$data = array(

			'country' => $this->input->post('country')

			);

		$this->country_id = $this->country_model->insert_if_not_exists('countries', $data);

	}

	function insert_location()
	{

		$this->load->model('location_model');

		$data = array(

			'lat'             => number_format($this->input->post('lat'), 14),

			'lng'             => number_format($this->input->post('lng'), 14),

			'streets_id'      => $this->street_id,

			'doors_number_id' => $this->door_number_id,

			'parishes_id'     => $this->parish_id,

			'cities_id'       => $this->city_id,

			'countries_id'    => $this->country_id 

			);

		$this->location_id = $this->location_model->insert_if_not_exists('locations', $data);

	}

	function insert_dates()
	{

		$this->load->model('date_model');

		$data = array(

			'date' => $this->input->post('date'),

			'time' => $this->input->post('start_time')

			);

		$start_date = $this->date_model->prep_date($data);

		$data = array(

			'date_time' => $start_date

			);

		$this->start_date_id = $this->date_model->insert_if_not_exists('dates_times', $data);		

		$data = array(

			'date' => $this->input->post('date'),

			'time' => $this->input->post('end_time')

			);


		$end_date = $this->date_model->prep_date($data);

		$data = array(

			'date_time' => $end_date

			);

		$this->end_date_id = $this->date_model->insert_if_not_exists('dates_times', $data);

	}

	function insert_venue()
	{

		$this->load->model('venue_model');

		$data = array(

			'nome' => $this->input->post('venue_name'),

			'locations_id' => $this->location_id

			);

		$this->venue_id = $this->venue_model->insert_if_not_exists('venues', $data);

	}

	function insert_event()
	{

		$this->load->helper('date');

		$this->load->model('event_model');

		$this->get_num_vagas();

		$this->get_automatic_public_date();

		$data = array(

			'locations_id' => $this->location_id,

			'organizer_id' => 15,

			'sports_id' => $this->sport_id,

			'max_players' => $this->input->post('max_jogadores'),

			'tipos_de_participacao_id' => $this->input->post('tipo_de_participacao'),

			'comentario_privado' => $this->input->post('comments_privado'),

			'comentario_publico' => $this->input->post('comments_publico'),

			'public_in_date_id' => $this->public_in_date_id,

			'vagas_privadas' => $this->num_vagas_privadas,

			'vagas_publicas' => $this->num_vagas_publicas,

			'sexo' => $this->input->post('sexo')

			);

		$this->event_id = $this->event_model->insert_if_not_exists('events', $data);


	}

	function get_num_vagas()
	{

		switch ($this->input->post('tipo_de_participacao')) {
			case '1': // Privado

				$this->num_vagas_privadas = $this->input->post('max_jogadores');

				$this->num_vagas_publicas = '0';

				break;

			case '2': // Publico

				$this->num_vagas_privadas = '0';

				$this->num_vagas_publicas = $this->input->post('max_jogadores');

				break;

			case '3': // Misto

				if ($this->input->post('vagas_publicas_automaticas_checkbox') == 'false') {

					$this->num_vagas_privadas = $this->input->post('max_jogadores') - $this->input->post('num_vagas_publicas');

					$this->num_vagas_publicas = $this->input->post('num_vagas_publicas');

				} else {

					$this->num_vagas_privadas = $this->input->post('max_jogadores');

					$this->num_vagas_publicas = '0';
				

				}
				
				break;	
				
		}


	}

	function get_automatic_public_date()
	{

		switch ($this->input->post('tipo_de_participacao')) {

			case '1': // Privado

				$this->public_in_date = NULL;

				break;

			case '2': // Publico

				$this->public_in_date = now();

				break;

			case '3': // Misto

				if ($this->input->post('vagas_publicas_automaticas_checkbox') == 'false') {

					$this->public_in_date = now();

				} else {

					$data = array(

						'date' => $this->input->post('date'),

						'time' => $this->input->post('start_time') . ':00'

						);

					$public_start_date = $this->date_model->prep_date($data);

					$public_in_date = $this->date_model->subtract_date($public_start_date, '12:00:00');

					$data = array(

						'date_time' => $public_in_date		

						);

					$this->public_in_date_id = $this->date_model->insert_if_not_exists('dates_times', $data);
				
				}
				
				break;	
				
		}


	}



}

/* End of file creator.php */
/* Location: ./application/controllers/creator.php */