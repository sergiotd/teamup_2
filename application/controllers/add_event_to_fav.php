<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_Event_To_Fav extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->data['event_id'] = $this->input->post('id');	

   		$this->load->view('add_event_to_fav/template', $this->data);

   }

}

/* End of file Add_Event_To_Fav.php */
/* Location: ./application/controllers/Add_Event_To_Fav.php */