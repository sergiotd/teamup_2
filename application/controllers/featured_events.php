<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Featured_Events extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$data = array(

   			array(

   				'foto' => 'foto',

   				'athletic_level' => '4',

   				'sport'=> 'football',

   				'address_area' => 'Lisboa',

   				'feedback_percentage' => '62',

   				'site_player_level' => 'gold'

   				),

   			array(

   				'foto' => 'foto',

   				'athletic_level' => '4',

   				'sport'=> 'football',

   				'address_area' => 'Lisboa',

   				'feedback_percentage' => '62',

   				'site_player_level' => 'gold'

   				)

   			);

   		$output = '';

   		foreach ($data as $value) {

			$output .= $this->load->view('featured_events/template', $value, TRUE);

   		}

   		echo json_encode(array('html' => $output));

   		

   }

}

/* End of file Featured_Events.php */
/* Location: ./application/controllers/Featured_Events.php */