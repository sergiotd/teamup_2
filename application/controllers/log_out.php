<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Log_Out extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->session->sess_destroy();

   		$output['modal_header']    = 'LOG OUT';

   		$output['modal_body']      = $this->load->view('log_out/template', $this->data, TRUE);

   		$output['modal_functions'] = $this->load->view('log_out/functions', $this->data, TRUE);

   		echo json_encode($output);

   }

}

/* End of file Log_Out.php */
/* Location: ./application/controllers/Log_Out.php */