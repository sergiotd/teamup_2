<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map_Address_Viewer extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('map_address_viewer/template', $this->data);

   }

}

/* End of file map_address_viewer.php */
/* Location: ./application/controllers/map_address_viewer.php */