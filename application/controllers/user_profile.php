<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Profile extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->data['teamid'] = $this->input->post('teamid');

   		$output['modal_header'] = 'Nome do Utilizador';

   		$output['modal_body'] = $this->load->view('user_profile/template', $this->data, TRUE);

   		$output['modal_functions'] = $this->load->view('user_profile/functions', $this->data, TRUE);

   		echo json_encode($output);

   }

   public function active_event()
   {

   	$this->data['teamid'] = $this->input->post('teamid');

   	$this->load->view('user_profile/template', $this->data);



   }

}

/* End of file User_Profile.php */
/* Location: ./application/controllers/User_Profile.php */