<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modal extends MY_Controller {

	var $data = array();

   public function index()
   {

   		// $this->data['modal_header'] = $this->input->post('modal_header');

   		$this->load->view('modal/template', $this->data);

   }

}

/* End of file Modal.php */
/* Location: ./application/controllers/Modal.php */