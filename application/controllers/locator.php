<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locator extends MY_Controller {

	var $data = array();

	public function initialize()
	{

		$this->load->view('template', $this->data);

	}


	public function index()
	{

		$this->data['account_items'] = array(

			'My Invitations' => 'account_invitations_container', 

			'My Games' => 'account_my_games_container', 

			'My Availabilities' => 'account_my_availability_container', 

			);

		$this->load->view('locator/template_locator', $this->data);

	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */