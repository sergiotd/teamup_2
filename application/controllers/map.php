<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map extends MY_Controller {

	var $data = array();

	public function index()
	{

		$page_origin = $this->input->post('page_origin');

		$output = $this->load->view('map/map', array(), TRUE);

		echo $output;
	
	}

	public function get_events_in_bounds()
	{

		$coords = explode(',', $this->input->post('bounds_coords'));

		// Pergunta à BD quais os eventos nesta area

		$this->load->model('map_model');

		$data = array();

		$query = $this->map_model->get_rows('events', $data);

		$data = array();

		foreach ($query->result() as $row) {
			
			$data[] = array(

				'id' => $row->id,

				'lat' => $row->lat,

				'lng' => $row->lng

				);

		}

		echo json_encode($data);

	}

}

/* End of file map.php */
/* Location: ./application/controllers/map.php */