<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_sport extends MY_Controller {

	var $data = array();
	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function locator()
	{

		$this->load->model('search_sport_model');

		$selected_language_id = $this->session->userdata('languages_id');

		$query_condition = array(

			'languages_id' => $selected_language_id
	
		);

		$query = $this->search_sport_model->get_rows('sports', $query_condition);

		// $this->data['sports_list'] = array_to_jquery_array($query->result_array(), 'sport');

		$this->load->helper('array');

		$random_sport = random_element($query->result_array());

		$this->load->helper('inflector');

		$this->data['sport'] = humanize($random_sport['sport']);

		$this->data['sport_id'] = $random_sport['id'];

		$this->load->view('search_sport/search_sport', $this->data);

	}

	public function creator()
	{

		$this->load->view('search_sport/search_sport', $this->data);

	}

}

/* End of file search_sport.php */
/* Location: ./application/controllers/search_sport.php */