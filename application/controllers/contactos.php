<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactos extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('contactos/template', $this->data);

   }

}

/* End of file Contactos.php */
/* Location: ./application/controllers/Contactos.php */