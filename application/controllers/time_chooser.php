<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Time_chooser extends MY_Controller {

   var $data = array(); 	

   public function index()
   {

      $this->load->view('time_chooser/time_chooser', $this->data);

   }

}

/* End of file time_chooser.php */
/* Location: ./application/controllers/time_chooser.php */