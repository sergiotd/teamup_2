<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pub extends MY_Controller {

	var $data = array();

   public function pub_skyscraper_span2()
   {

   		$this->load->view('pub/pub_skyscraper_span2', $this->data);

   }

   public function pub_row()
   {

   		$this->load->view('pub/pub_row', $this->data);

   }

}

/* End of file Pub.php */
/* Location: ./application/controllers/Pub.php */