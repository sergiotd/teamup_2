<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('empresa/template', $this->data);

   }

}

/* End of file empresa.php */
/* Location: ./application/controllers/empresa.php */