<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_Directions extends MY_Controller {

	var $data = array();

   public function index()
   {

		$this->data['lat'] = '38.662363'; 

		$this->data['lng'] = '-9.221348';

		$this->data['static_map'] = 'http://maps.googleapis.com/maps/api/staticmap?';

		$this->data['static_map'] .= 'center='. $this->data['lat'] . ','. $this->data['lng'] .'&';

		$this->data['static_map'] .= 'zoom=15&';

		$this->data['static_map'] .= 'size=200x200&';

		$this->data['static_map'] .= 'markers=color:blue%7C';

		$this->data['static_map'] .= $this->data['lat'] .','. $this->data['lng'] .'&';

		$this->data['static_map'] .= 'markers=size:tiny%7C';

		$this->data['static_map'] .= 'color:green&';

		$this->data['static_map'] .= 'sensor=false';

		$this->load->view('event_directions/template', $this->data);

   }

}

/* End of file Event_Directions.php */
/* Location: ./application/controllers/Event_Directions.php */