<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportar_Problemas extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('reportar_problemas/template', $this->data);

   }

}

/* End of file reportar_problemas.php */
/* Location: ./application/controllers/reportar_problemas.php */