<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_Info extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('venue_info/template', $this->data);

   }

}

/* End of file venue_info.php */
/* Location: ./application/controllers/venue_info.php */