<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends MY_Controller {

   var $data = array();

   public function index()
   {



         $people = array(

            array(

               'user_id' => '1524',

               'foto' => '',

               'complete_name' => 'Sérgio Duarte',

               'age' => '35',

               'sex' => 'm',

               'sport' => 'Futebol',

               'address_area' => 'benfica',

               'athletic_level' => '4',

               'feedback_percentage' => '62',

               'participation' => '11',

               'site_reg_date' => '21/12/2007',

               'comment' => 'I wanna Play Football!',

               'site_player_level' => 'gold'

               ),

            array(

               'user_id' => '1524',

               'foto' => '',

               'complete_name' => 'Sérgio Duarte',

               'age' => '35',

               'sex' => 'm',

               'sport' => 'Futebol',

               'address_area' => 'benfica',

               'athletic_level' => '4',

               'feedback_percentage' => '62',

               'participation' => '11',

               'site_reg_date' => '21/12/2007',

               'comment' => 'I wanna Play Football!',

               'site_player_level' => 'gold'

               )


            
   
            );



         $output = '';

         foreach ($people as $value) {

            $output .= $this->load->view('people/people_card_list', $this->data, TRUE);

         }


         echo json_encode(array('html' => $output));

   }

   public function update()
   {

         $this->load->model('people_model');

         $sport = $this->input->post('sport');

         $map_bounds = $this->input->post('map_bounds');

         $query = $this->people_model->get_available_users($sport, $map_bounds);

         $this->data['loners_list'] = $query;

         $this->load->view('people/people_list', $this->data);

   }

}

/* End of file people.php */
/* Location: ./application/controllers/people.php */