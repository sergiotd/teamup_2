<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info_Media extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('info_media/template', $this->data);

   }

}

/* End of file info_media.php */
/* Location: ./application/controllers/info_media.php */