<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {

	var $data = array();

   public function log_in_success()
   {

   		$this->load->view('feedback/log_in_success', $this->data);

   }

   public function sign_up_success()
   {

   		$this->load->view('feedback/sign_up_success', $this->data);


   }

}

/* End of file Feedback.php */
/* Location: ./application/controllers/Feedback.php */