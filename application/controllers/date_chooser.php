<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Date_Chooser extends MY_Controller {

	var $data = array();

	public function __construct()
	{
	   parent::__construct();
	}

	public function index()
	{

		$this->load->helper('date');

		$date_string = "%d-%m-%Y";

		$this->data['current_date'] = mdate($date_string);

		$this->load->view('date_chooser/date_chooser', $this->data);

	}

}

/* End of file date_chooser.php */
/* Location: ./application/controllers/date_chooser.php */