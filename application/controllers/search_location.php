<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_Location extends MY_Controller {

	var $data = array();

   public function index()
   {

   		//$this->data['main_view'] = 'search_location';

	   	$this->data['search_button_html_class'] = $this->input->post('origin');

   		$this->load->view('search_location/search_location', $this->data);

   }

}

/* End of file Search_Location.php */
/* Location: ./application/controllers/Search_Location.php */