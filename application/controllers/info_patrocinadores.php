<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info_Patrocinadores extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('info_patrocinadores/template', $this->data);

   }

}

/* End of file info_patrocinadores.php */
/* Location: ./application/controllers/info_patrocinadores.php */