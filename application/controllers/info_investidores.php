<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info_Investidores extends MY_Controller {

	var $data = array();

   public function index()
   {

   		$this->load->view('info_investidores/template', $this->data);

   }

}

/* End of file info_investidores.php */
/* Location: ./application/controllers/info_investidores.php */