<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MY_Controller {

	var $data = array();

	public function index()
	{

		$this->load->model('cart_model');

		$user_id = $this->session->userdata('user_id');

		$this->data['query'] = $this->cart_model->get_all_favorites($user_id);

		$this->load->view('cart/template', $this->data);		
		
	}

}

/* End of file cart.php */
/* Location: ./application/controllers/cart.php */