<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	function array_to_dropdown($raw_data, $key_field, $value_field)
	{

		$data = array();

		foreach($raw_data as $raw)
		{

			if (! is_array($value_field)) {

			$data[$raw[$key_field]] = $raw[$value_field];

			} else {

				foreach ($value_field as $v) {

					$data[$raw[$key_field]][$v] = $raw[$v]; 

				}

			}

		}

		return $data;
	  
	}

	function array_to_jquery_array($raw_data = array(), $value_key = '')
	{

		$output_str = "[";

		$last_element_in_array = end($raw_data);

		foreach ($raw_data as $value) {

			$output_str .= "\"".$value[$value_key]."\"";

			if ($value != $last_element_in_array) {

				$output_str .= ",";			

			}
					
		}

		$output_str .= "]";

		return $output_str;
	
	}

?>