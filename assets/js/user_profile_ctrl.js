function show_active_event_view_user_profile(button)
{

	$('<div class="well"><div class="well-white" id="active_event_user_profile" style="display:none"></div></div>')

		.insertAfter($(button).closest('.row-fluid'));

	$('#active_event_user_profile').load('user_profile/active_event',{teamid: 2}, function(){

		$('#active_event_user_profile').slideDown('fast');

	});

}

$('.active_event_view_user_profile').live('click', function(e){

	if ($('#active_event_user_profile').html() === null) {

		$(this).button('toggle');

		show_active_event_view_user_profile($(this));

	}else{

		if ($(this).hasClass("active") === true) {

			$('#active_event_user_profile').slideUp('fast');

			$('#active_event_user_profile').parent().remove();

			$('#active_event_user_profile').remove();

			$(this).button('toggle');

		}else{

			$('.active_event_view_user_profile.active').removeClass('active');

			$('#active_event_user_profile').remove();

			show_active_event_view_user_profile($(this));

			$(this).button('toggle');

		}



	}

	e.stopImmediatePropagation();

});

$('#abrir_vagas_publicas_div_button').live('click', function(){

	$('#abrir_vagas_publicas_div').slideToggle('fast');

	$(this).button('toggle');


});

