var map_object = {};

var markers = [];

var circles = [];

/* Inicializa o mapa da pagina locator */

function initialize_map (callback, callback_1) {

	map_object = {};

	if(navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(function(position) {

			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			var myOptions = {

				zoom: 14,

				center: pos,

				mapTypeId: google.maps.MapTypeId.ROADMAP

			};

			map_object = new google.maps.Map(document.getElementById('map'), myOptions);

			callback();

			callback_1();

		}, function() {});

	} else {

		// Browser doesn't support Geolocation

	}

}

function initialize_locator_map()
{

	initialize_map(define_listener_map_dragend, define_listener_map_tilesloaded);

}

function initialize_creator_active_map()
{

	initialize_map();

	markers = [];

	add_new_marker(pos);
	
	get_geocoder_address_by_latLng(pos);

}

function initialize_creator_passive_map()
{

	initialize_map();

	markers = [];

	add_new_marker(pos);

	set_radius(markers[0]);

	get_geocoder_address_by_latLng(pos);


}

function update_map_bounds(latlng, formatted_address)
{

	map_object.panTo(latlng);

	update_slider_header('#where_container', formatted_address);

}

function define_listener_map_dragend()
{

	google.maps.event.addListener(map_object, 'dragend', function(e){

		var geocoder = new google.maps.Geocoder();

		geocoder.geocode({'location': map_object.getCenter()}, function(results, status){

			update_location_input(results[0].formatted_address);

			update_slider_header('#where_container', results[0].formatted_address);
		
			$('#main_search_locator_button').trigger('click');

		});

	});

}

function define_listener_map_tilesloaded()
{

	google.maps.event.addListener(map_object, 'tilesloaded', function(e){

		var geocoder = new google.maps.Geocoder();

		geocoder.geocode({'location': map_object.getCenter()}, function(results, status){

			update_location_input(results[0].formatted_address);

			update_slider_header('#where_container', results[0].formatted_address);
		
			$('#main_search_locator_button').trigger('click');

		});

	});

}

function populate_map(data)
{

	markers = [];

	$.post('map/get_events_in_bounds',

		{
		
			sport: data.sport,

			date: data.date,

			start_time: data.start_time,

			end_time: data.end_time,

			filters: data.filters,

			bounds_coords: data.bounds_coords

		},

		function(data){

			$.each(data, function(){

				var eventid = this.id;

				var info = this.info;

				var lat = this.lat;

				var lng = this.lng;

				var marker_temp = new google.maps.Marker({

					position: new google.maps.LatLng(lat, lng),

					map: map_object,

					flat: true,

					title: info,

					draggable: true,

					eventid: eventid

				});

				markers.push(marker_temp);

				define_listener_map_marker_click();

			});

	}, 'json');

}


// function add_new_marker (location) {

// 	var marker = new google.maps.Marker({

// 		position: location,

// 		title:"My New Event",

// 		draggable: true

// 	});

// 	marker.setMap(map_object);

// 	markers.push(marker);

// 	google.maps.event.addListener(marker, 'dragend', function(e){

// 		var location = marker.getPosition();

// 		get_geocoder_address_by_latLng(location);

// 	});

// }

function update_people(data)
{

	$('#people_main_div').load('people/update',
	{

		sport: data.sport,

		date: data.date,

		start_time: data.start_time,

		end_time: data.end_time,

		filters: data.filters,

		lat_hi: data.map_bounds.lat_hi,

		lat_lo: data.map_bounds.lat_lo,

		lng_hi: data.map_bounds.lng_hi,

		lng_lo: data.map_bounds.lng_lo

	}, function(data){



	});

}

function update_pub(data)
{

	$('#pub_right').load('pub',
	{

		sport: data.sport


	}, function(data){



	});


}

function split_lat_lng(latlng_obj)
{

	var output = {

		lat: latlng_obj.lat(),

		lng: latlng_obj.lng()

	};

	return output;

}

function get_latlng_from_address(string, callback)
{

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({'address': string}, function(results, status){

		var latlng = split_lat_lng(results[0].geometry.location);

		callback(results, latlng);

	});

}

// function get_geocoder_address_by_latLng(latLng)
// {
	
// 	var geocoder = new google.maps.Geocoder();

// 	geocoder.geocode({'location': latLng}, function(results, status){

// 		get_address(results, latLng);
	
// 	});

// }


// function get_geocoder_address_by_string(string)
// {
	
// 	var geocoder = new google.maps.Geocoder();

// 	geocoder.geocode({'address': string}, function(results, status){

// 		var latLng = results[0].geometry.location;

// 		get_address(results, latLng);
	
// 	});

// }

function get_address_components(results)
{

	var output = {

		'lat' : results[0].geometry.location.lat(),

		'lng' : results[0].geometry.location.lng()

	};

	for (var j=0; j<results.length; j++)
	{

		for (var i=0; i<results[j].address_components.length; i++)
		{

			if (results[j].address_components[i].types[0] == "route") {

				if (output['street']=== undefined) {output['street'] = results[j].address_components[i].long_name;}

			}

			if (results[j].address_components[i].types[0] == "street_number") {

				if (output['doorNumber'] === undefined) {output['doorNumber'] = results[j].address_components[i].long_name;}

			}

			if (results[j].address_components[i].types[0] == "administrative_area_level_1") {

				if (output['city'] === undefined) {output['city'] = results[j].address_components[i].long_name;}

			}
			if (results[j].address_components[i].types[0] == "administrative_area_level_3") {

				if (output['parish']=== undefined) {output['parish'] = results[j].address_components[i].long_name;}

			}
			if (results[j].address_components[i].types[0] == "country") {

				if (output['country']=== undefined) {output['country'] = results[j].address_components[i].long_name;}

			}

		}

	}

	if (output.city === undefined) {output['city'] = '';}

	if (output.country === undefined) {output['country'] = '';}

	if (output.parish === undefined) {output['parish'] = '';}

	if (output.doorNumber === undefined) {output['doorNumber'] = '';}
	
	if (output.street === undefined) {output['street'] = '';}

	// if ($('#search_location_button.creator').length !== 0) {

	// 	map_object.panTo(latLng);

	// 	if (markers.length === 0) {

	// 		markers.push(add_new_marker(latLng));

	// 	}else{

	// 		markers[0].setPosition(latLng);

	// 	}

	// 	populate_map_address_viewer(output);

	// }

	set_search_location_input_data(output);

}

function define_listener_map_marker_click()
{

	google.maps.event.addListener(marker_temp, 'click', function(event) {

		// var string = event.latlng.toUrlValue();

		$('#modal_container').load('modal', {}, function(data){

			$.post('event_profile/marker', {id: marker_temp.eventid}, function(data2){

				$('#myModal #modal_header').html(data2.modal_header);

				$('#myModal .modal-body').html(data2.modal_body);

				$('#myModal #modal_functions').html(data2.modal_functions);

				$('#event_directions').load('event_directions',{eventid:eventid}, function(){

					var autoc_directions = new google.maps.places.Autocomplete(document.getElementById('location_start'));

					$('.pac-container').css('z-index', '9999');

				});

				$('#modal_pub_left').load('pub/pub_row');

				$('#modal_pub_right').load('pub/pub_row');

				$('#myModal').modal('show');

			}, 'json');

		});

	});

}