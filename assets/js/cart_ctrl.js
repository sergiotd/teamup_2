$('.cart_events').live('click', function(e){

	var eventid = $(this).parent().attr('data-eventid');

	$('#modal_container').load('modal', {}, function(data){

		$.post('event_profile/cart', {eventid: eventid}, function(data2){

			$('#myModal #modal_header').html(data2.modal_header);

			$('#myModal .modal-body').html(data2.modal_body);

			$('#myModal #modal_functions').html(data2.modal_functions);

			$('#event_directions').load('event_directions',{eventid:eventid}, function(){

				var autoc_directions = new google.maps.places.Autocomplete(document.getElementById('location_start'));

				$('.pac-container').css('z-index', '9999');

			});

			$('#modal_pub_left').load('pub/pub_row');

			$('#modal_pub_right').load('pub/pub_row');

			$('#myModal').modal('show');

		}, 'json');

	});

	e.preventDefault();

});

