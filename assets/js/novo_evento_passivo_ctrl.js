function construct_new_passivo()
{
	
	$('#novo_evento_container').load('creator/novo_evento_passivo', function(){

		$('#search_sport').load('search_sport/creator', function(){});

		$('#date_chooser').load('date_chooser', function(){});

		$('#time_chooser').load('time_chooser', function(){});

		$('#radius_buttons').load('radius_buttons', function(){

			$('#map_canvas').load('map',{}, function(){

				$('#search_location').load('search_location',{origin: 'creator'}, function(){

					var autoc = new google.maps.places.Autocomplete(document.getElementById('search_location_input'));

					initialize_map('creator_passivo', 14);

				});

			});

		});

		$('#extra_filtering').load('extra_event_filtering', function(){});

		$('#map_address_viewer').load('map_address_viewer', function(){});

	});

}

$('#form_novo_evento_passivo').live('submit', function(e){

	var link = $(this).attr('action');

	$.post(link,
		{

			sport: $('#search_sport_input').val(),

			date: $('.data_chooser').find('input').val(),

			start_time: $('.timepicker.start').val(),

			end_time: $('.timepicker.end').val(),

			lat: $('#search_location_input').attr('data-lat'),

			lng: $('#search_location_input').attr('data-lng'),

			street: $('#search_location_input').attr('data-street'),

			door_number: $('#search_location_input').attr('data-doornumber'),

			parish: $('#search_location_input').attr('data-parish'),

			city: $('#search_location_input').attr('data-city'),

			country: $('#search_location_input').attr('data-country'),

			radius: $('#radius_buttons').find('button.active').attr('data-value'),

			faixas_etarias: $('input[name="faixa_etaria"]:checked').map(function(){ return $(this).val(); }).get().join(","),

			sexo: $('input[name="sexo"]').val(),

			amigos: $('input[name="social_network"]:checked').map(function(){ return $(this).val(); }).get().join(","),

			comments: $('textarea[name="comments"]').val()

		}, function(data){

			process_new_event(data, 'creator/novo_evento_passivo_submit_success');

		}, 'json');

	e.preventDefault();

});