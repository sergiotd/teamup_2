$('#get_directions_button').live('click', function(){

	$('#directions_panel').empty();

	var location_start = $('#location_start').val();

	var dest_lat = $(this).attr('data-dest-lat');

	var dest_lng = $(this).attr('data-dest-lng');

	var destination = new google.maps.LatLng(dest_lat, dest_lng);

	var directions_obj = new google.maps.DirectionsService();

	var directionsRenderer = new google.maps.DirectionsRenderer();

	directionsRenderer.setPanel(document.getElementById('directions_panel'));

	var request = {

		origin: location_start,

		destination: destination,

		travelMode: google.maps.DirectionsTravelMode.DRIVING

	};

	var directionsDisplay = new google.maps.DirectionsRenderer();

	directions_obj.route(request, function(result, status){

		directionsRenderer.setDirections(result);

		if (location_start !== '') {

			$('#directions_panel')

				.append('<button style="margin-bottom:10px" class="btn pull-right span12" type="button">Imprimir</button>');

		}


	});

});

