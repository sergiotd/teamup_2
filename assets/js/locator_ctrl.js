function construct_locator()
{

	$('#main_view').load('locator', {}, function(){

		$('#map_container')

			.load(

				'slider_container',

				{

					toggle_div_id: 'map_canvas',

					open: 'true',

					slider_title: 'Sports',

					slider_sub_title: 'Amateur',

					slider_bullet_color: 'orange'

				},

				function(){

				$('#map_canvas')

					.load('map', function(){

						initialize_locator_map();

					});

			});

		$('#people_container')

			.load(

				'slider_container',

				{

					toggle_div_id: 'people_list',

					open: 'true',

					slider_title: 'Players',

					slider_sub_title: 'Available',

					slider_bullet_color: 'blue'

				},

				function(){

				$.post(

					'people',

					{},

					function(data){

						console.log(data.html);

						$('#people_list').append(data.html);

					}, 'json');

			});

		// $('#account_invitations_container')
			
		// 	.load('slider_container', {toggle_div_id: 'account_invitations'}, function(){



		// 	});

		// $('#account_my_games_container')
			
		// 	.load('slider_container', {toggle_div_id: 'account_my_games'}, function(){



		// 	});

		// $('#account_my_availability_container')
			
		// 	.load('slider_container', {toggle_div_id: 'account_my_availability'}, function(){



		// 	});
			
		$('#account_my_favorites_container')
			
			.load('slider_container', {toggle_div_id: 'account_my_favorites'}, function(){



			});

			$.post(

				'featured_events',

				{},

				function(data){

					$('#featured_container').append(data.html);

				}, 'json');

	});

}

function update_locator(results, latlng)
{

	var data = {

		sport : $('#search_sport_input').val(),

		date : $('.data_chooser').find('input').val(),

		start_time : $('.timepicker.start').val(),

		end_time : $('.timepicker.end').val(),

		filters: get_filters()

	};

	data['lat'] = latlng.lat;

	data['lng'] = latlng.lng;

	data['map_bounds'] = map_object.getBounds().toUrlValue();

	if ($('#search_location_input').attr('data-lat') != latlng.lat && $('#search_location_input').attr('data-lng') != latlng.lng) {
	
		update_map_bounds(results[0].geometry.location, results[0].formatted_address);

	}

	get_address_components(results);

	populate_map(data);

	// populate_people(data);

}

$('#main_search_locator_button').live('click', function(){

	get_latlng_from_address($('#search_location_input').val(), update_locator);

});

function update_location_input(formatted_address)
{

	$('#search_location_input').val(formatted_address);

}