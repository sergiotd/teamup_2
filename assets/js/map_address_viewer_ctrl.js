function populate_map_address_viewer (address_data) {

	var treated_street =  '';

	if (address_data.doorNumber === '' ) {

		treated_street = address_data.street;
		
	}else if(address_data.street === ''){

		treated_street = '';

	}else if(address_data.street === '' && address_data.doorNumber === ''){

		treated_street = '';

	}else{

		treated_street = address_data.street + ', ' + address_data.doorNumber;

	}
		
	$('#map_address_viewer_street').html(treated_street);

	$('#map_address_viewer_parish').html(address_data.parish);

	$('#map_address_viewer_city').html(address_data.city);

	$('#map_address_viewer_country').html(address_data.country);

	$('#map_address_viewer_gps_coordinates').html(address_data.lat + ' ; ' + address_data.lng);

}