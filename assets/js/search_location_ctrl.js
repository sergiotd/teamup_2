function set_search_location_input_data (data) {

	$('#search_location_input').attr('data-city', data.city);

	$('#search_location_input').attr('data-country', data.country);

	$('#search_location_input').attr('data-doornumber', data.doorNumber);

	$('#search_location_input').attr('data-parish', data.parish);

	$('#search_location_input').attr('data-street', data.street);

	$('#search_location_input').attr('data-lat', data.lat);

	$('#search_location_input').attr('data-lng', data.lng);

}
