function construct_creator()
{

	$('#main_view').load('creator', function(){

		$('#pub_left').load('pub/pub_skyscraper_span2', {}, function(){});

		$('#pub_right').load('pub/pub_skyscraper_span2', {}, function(){});

	});

}

$('select[name="tipo_de_evento"]').live('change', function(){

	var tipo_de_evento = $(this).val();

	if (tipo_de_evento == 'activo') { construct_new_activo(); }

	if (tipo_de_evento == 'passivo') { construct_new_passivo(); }

});


function process_new_event(data, form_success_ctrl)
{

	if (data.result === false) {

		$.each(data, function(name, value){

			if (name !== 'result') {

				$('#form_errors').append('<p>' + value + '</p>');

			}


		});

	}else{

		$('#modal_container').load('modal', {}, function(data){

			$.post(form_success_ctrl, {}, function(data2){

				$('#myModal #modal_header').html(data2.modal_header);

				$('#myModal .modal-body').html(data2.modal_body);

				$('#myModal #modal_functions').html(data2.modal_functions);

				$('#modal_pub_left').load('pub/pub_row');

				$('#modal_pub_right').load('pub/pub_row');

				$('#myModal').modal('show');

			}, 'json');

		});

		$('#locator').trigger('click');

	}

}