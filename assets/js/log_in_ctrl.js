function construct_log_in()
{

	$('#modal_container').load('modal', {}, function(data){

		$.post('log_in', {}, function(data2){

			$('#myModal #modal_header').html(data2.modal_header);

			$('#myModal .modal-body').html(data2.modal_body);

			$('#myModal #modal_functions').html(data2.modal_functions);

			$('#modal_pub_left').load('pub/pub_row');

			$('#modal_pub_right').load('pub/pub_row');

			$('#myModal').modal('show');

		}, 'json');

	});

}

$('#log_in_submit_button').live('click', function(){

	$('#log_in_form').submit();

});

$('#log_in_form').live('submit', function(e){

	var $this = $(this);

	var link = $('#log_in_form').attr('action');

	$.post(link,

		{

			email: $('#log_in_form input[name="email"]').val(),

			password: $('#log_in_form input[name="password"]').val()


		}, function(data){

			if (data == 'true') {

				$this.closest('.modal-body').load('feedback/log_in_success', {}, function(){

					//desativar todos os botoes

					$('#log_in_submit_button').prop('disabled', 'disabled');

					$('#log_in_cancel').html('OK');

					$('#home').trigger('click', function(){});

				});

			}else{

				$this.closest('.modal-body')

					.empty()

					.append(data);

			}

	}, 'html');

	e.preventDefault();

});