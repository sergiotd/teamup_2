function set_radius () {

	var radius = $('.radius_buttons.active').attr('data-value');

	circles[0] = new google.maps.Circle({

		map: map_object,

		radius: parseInt(radius, 10),

		fillColor: 'blue'

	});



	circles[0].bindTo('center', markers[0], 'position');

	set_map_zoom_radius(radius);

}

function set_map_zoom_radius (radius) {

	if (radius == '1000') {map_object.setZoom(14);}

	if (radius == '2000') {map_object.setZoom(13);}

	if (radius == '3000') {map_object.setZoom(12);}

	if (radius == '5000') {map_object.setZoom(11);}

	if (radius == '10000') {map_object.setZoom(10);}

}

$('.radius_buttons').live('click', function(){

	circles[0].setMap(null);

	circles = [];

	set_radius();

});