function get_filters()
{

	var string = {

		faixa_etaria: $('input[name="faixa_etaria"]:checked').map(function(){ return $(this).val(); }).get().join(","),

		social_network: $('input[name="social_network"]:checked').map(function(){ return $(this).val(); }).get().join(",")

	};

	return string;

}