$('.main_navigation a').live('click', function(e){

	var link = $(this).attr('href');

	var id = $(this).attr('id');

	$this = $(this);

	$('.nav li[class~="active"]').removeClass('active');

	$this.parent().addClass('active');

	// if (id == 'home') { construct_home(); }

	if (id == 'locator') { construct_locator(); }

	if (id == 'creator') { construct_creator(); }

	if (id == 'about') { construct_about(); }

	if (id == 'my_account') { construct_my_account(); }

	if (id == 'contactos') { construct_contactos(); }

	if (id == 'sugestoes') { construct_sugestoes(); }

	if (id == 'reportar_problemas') { construct_reportar_problemas(); }

	if (id == 'info_media') { construct_info_media(); }

	if (id == 'info_investidores') { construct_info_investidores(); }

	if (id == 'info_patrocinadores') { construct_info_patrocinadores(); }

	if (id == 'empresa') { construct_empresa(); }

	if (id == 'footer_home') { $('#home').trigger('click'); }

	if (id == 'footer_creator') { $('#creator').trigger('click'); }

	if (id == 'footer_locator') { $('#locator').trigger('click'); }

	if (id == 'footer_about') { $('#about').trigger('click'); }

	if (id == 'footer_my_account') { $('#my_account').trigger('click'); }

	if (id == 'log_in') {construct_log_in(); }

	if (id == 'log_out') {construct_log_out();}

	if (id == 'sign_up') {construct_sign_up();}

	e.preventDefault();

});

