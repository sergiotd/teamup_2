$('.modal_links').live('click', function(e){

	var id = $(this).attr('data-eventid');

	var link = $(this).attr('href');

	var modal_header = $(this).attr('data-title');

	$.post(link, {id: id}, function(data, textStatus, xhr) {

		$('#modal_container').load('modal', {modal_header: modal_header}, function(){

			$('#myModal').find('.modal-body').html(data);

			$('#myModal').modal('show');

		});

	});
	
	e.preventDefault();

});

$('#myModal').live('hidden', function () {

	$('#modal_container').empty();

});

