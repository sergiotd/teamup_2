function construct_sign_up()
{

	$('#modal_container').load('modal', {}, function(data){

		$.post('sign_up', {}, function(data2){

			$('#myModal #modal_header').html(data2.modal_header);

			$('#myModal .modal-body').html(data2.modal_body);

			$('#myModal #modal_functions').html(data2.modal_functions);

			$('#modal_pub_left').load('pub/pub_row');

			$('#modal_pub_right').load('pub/pub_row');

			$('#myModal').modal('show');

		}, 'json');

	});

}

$('#sign_up_submit_button').live('click', function(){

	$('#sign_up_form').submit();

});

$('#sign_up_form').live('submit', function(e){

	var $this = $(this);

	var link = $('#sign_up_form').attr('action');

	$.post(link,

		{

			first_name: $('#sign_up_form input[name="first_name"]').val(),

			last_name: $('#sign_up_form input[name="last_name"]').val(),

			avatar: $('#sign_up_form input[name="avatar"]').val(),

			email: $('#sign_up_form input[name="email"]').val(),

			default_country_id: $('#sign_up_form select[name="default_country"]').val(),

			default_language_id: $('#sign_up_form select[name="default_language"]').val(),

			password: $('#sign_up_form input[name="password"]').val(),

			password_conf: $('#sign_up_form input[name="password_conf"]').val(),

			faixa_etaria: $('#sign_up_form select[name="faixa_etaria"]').val()


		}, function(data){

			if (data == 'true') {

				$this.closest('.modal-body').load('feedback/sign_up_success', {}, function(){

					//desativar todos os botoes

					$('#sign_up_submit_button').prop('disabled', 'disabled');

					$('#sign_up_cancel_button').html('OK');

				});

			}else{

				$this.closest('.modal-body')

					.empty()

					.append(data);

			}

	}, 'html');

	e.preventDefault();

});

