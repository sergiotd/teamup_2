function construct_log_out()
{

	$('#modal_container').load('modal', {}, function(data){

		$.post('log_out', {}, function(data2){

			$('#myModal #modal_header').html(data2.modal_header);

			$('#myModal .modal-body').html(data2.modal_body);

			$('#myModal #modal_functions').html(data2.modal_functions);

			$('#modal_pub_left').load('pub/pub_row');

			$('#modal_pub_right').load('pub/pub_row');

			$('#myModal').modal('show');

		}, 'json');

	});
}


$('#log_out_ok_button').live('click', function(){

	$('#home').trigger('click');

});

