function construct_new_activo()
{

	$('#novo_evento_container').load('creator/novo_evento_activo', function(){

		$('#search_sport').load('search_sport/creator', function(){});

		$('#date_chooser').load('date_chooser', function(){});

		$('#time_chooser').load('time_chooser', function(){});

		$('#venue_info').load('venue_info', function(){});

		$('#map_canvas').load('map',{}, function(){

			$('#search_location').load('search_location',{origin: 'creator'}, function(){

				var autoc = new google.maps.places.Autocomplete(document.getElementById('search_location_input'));

				initialize_map('creator_activo', 14);

			});

		});

		$('#extra_filtering').load('extra_event_filtering', function(){});

		$('#map_address_viewer').load('map_address_viewer', function(){});

	});

}

$('#form_novo_evento_activo').live('submit', function(e){

	var link = $(this).attr('action');

	$.post(link,
		{

			sport: $('#search_sport_input').val(),

			date: $('.data_chooser').find('input').val(),

			start_time: $('.timepicker.start').val(),

			end_time: $('.timepicker.end').val(),

			lat: $('#search_location_input').attr('data-lat'),

			lng: $('#search_location_input').attr('data-lng'),

			street: $('#search_location_input').attr('data-street'),

			door_number: $('#search_location_input').attr('data-doornumber'),

			parish: $('#search_location_input').attr('data-parish'),

			city: $('#search_location_input').attr('data-city'),

			country: $('#search_location_input').attr('data-country'),

			venue_name: $('input[name="local_nome"]').val(),

			max_jogadores: $('input[name="max_jogadores"]').val(),

			tipo_de_participacao: $('select[name="tipos_de_participacao_dropdown"]').val(),

			comments_privado: $('textarea[name="notas_evento_privado"]').val(),

			vagas_publicas_automaticas_checkbox: $('input[name="abrir_vagas_publicas_automaticas"]').prop('checked'),

			num_vagas_publicas: $('input[name="num_vagas_publicas"]').prop('checked'),

			faixas_etarias: $('input[name="faixa_etaria"]:checked').map(function(){ return $(this).val(); }).get().join(","),

			sexo: $('input[name="sexo"]').val(),

			amigos: $('input[name="social_network"]:checked').map(function(){ return $(this).val(); }).get().join(","),

			comments_publico: $('textarea[name="notas_evento_privado"]').val()

		}, function(data){

			process_new_event(data, 'creator/novo_evento_activo_submit_success');

		}, 'json');

	e.preventDefault();

});

$('input[name="abrir_vagas_publicas_automaticas"]').live('change', function(){

	

	if ($(this).prop('checked') === true) {

		$('#num_vagas_publicas_div').slideUp();

	}else{

		$('#num_vagas_publicas_div').slideDown();

	}

});

$('select[name="tipos_de_participacao_dropdown"]').live('change', function(){

	if ($(this).val() === '0') {

		$('#evento_privado').hide();

		$('#evento_publico').hide();

	}

	if ($(this).val() === '1') {

		$('#evento_privado').show();

		$('#evento_publico').hide();

	}

	if ($(this).val() === '2') {

		$('#evento_privado').hide();

		$('#evento_only_public').hide();

		$('#evento_publico').show();

	}

	if ($(this).val() === '3') {

		$('#evento_privado').show();

		$('#evento_only_public').show();

		$('#evento_publico').show();

	}

});